package router

import (
	v1 "gin-luban-server/api/v1"
	"gin-luban-server/middleware"
	"github.com/gin-gonic/gin"
)

func InitCompanyRouter(Router *gin.RouterGroup) {
	SysCompanyRouter := Router.Group("company").Use(middleware.OperationRecord())
	{
		SysCompanyRouter.POST("createCompany", v1.CreateCompany)   // 添加公司
		SysCompanyRouter.PUT("updateCompany", v1.UpdateCompany)   // 更新公司
		SysCompanyRouter.DELETE("deleteCompany", v1.DeleteCompany) // 删除公司
		SysCompanyRouter.POST("getCompanyById", v1.GetCompanyById) // 通过Id获取公司信息
		SysCompanyRouter.GET("getCompany", v1.GetCompany)         // 获取公司信息
		SysCompanyRouter.GET("getCompanyList", v1.GetCompanyList) // 获取公司列表

	}
}
