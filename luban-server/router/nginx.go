package router

import (
	v1 "gin-luban-server/api/v1"
	"github.com/gin-gonic/gin"
)

func InitNginxRouter(Router *gin.RouterGroup) {
	// 证书
	NginxDomainCertRouter := Router.Group("Cert")
	{
		NginxDomainCertRouter.POST("createNginxDomainCert", v1.CreateNginxDomainCert)   // 新建NginxDomainCert
		NginxDomainCertRouter.DELETE("deleteNginxDomainCert", v1.DeleteNginxDomainCert) // 删除NginxDomainCert
		//NginxDomainCertRouter.DELETE("deleteNginxDomainCertByIds", v1.DeleteNginxDomainCertByIds) // 批量删除NginxDomainCert
		//NginxDomainCertRouter.PUT("updateNginxDomainCert", v1.UpdateNginxDomainCert)    // 更新NginxDomainCert
		NginxDomainCertRouter.POST("getNginxDomainCertById", v1.GetNginxDomainCertById)        // 根据ID获取NginxDomainCert
		NginxDomainCertRouter.POST("getNginxDomainCertList", v1.GetNginxDomainCertList)  // 获取NginxDomainCert列表
	}

	// Pool
	NginxPoolRouter := Router.Group("pool")
	{
		NginxPoolRouter.POST("createNginxPool", v1.CreateNginxPool)   // 新建NginxPool
		NginxPoolRouter.DELETE("deleteNginxPool", v1.DeleteNginxPool) // 删除NginxPool
		NginxPoolRouter.PUT("updateNginxPool", v1.UpdateNginxPool)    // 更新NginxPool
		NginxPoolRouter.POST("getNginxPoolById", v1.GetNginxPoolById)        // 根据ID获取NginxPool
		NginxPoolRouter.POST("getNginxPoolList", v1.GetNginxPoolList)  // 获取NginxPool列表
	}

	// PoolNode
	NginxPNodeRouter := Router.Group("PNode")
	{
		NginxPNodeRouter.POST("createNginxPNode", v1.CreateNginxPNode)   // 新建NginxPoolNode
		NginxPNodeRouter.DELETE("deleteNginxPNode", v1.DeleteNginxPNode) // 删除NginxPoolNode
		NginxPNodeRouter.PUT("updateNginxPNode", v1.UpdateNginxPNode)    // 更新NginxPoolNode
		NginxPNodeRouter.POST("getNginxPNodeById", v1.GetNginxPNodeById)        // 根据ID获取NginxPoolNode
		NginxPNodeRouter.POST("getNginxPNodeList", v1.GetNginxPNodeList)  // 获取NginxPoolNode列表
	}

}
