package router

import (
	v1 "gin-luban-server/api/v1"
	"gin-luban-server/middleware"
	"github.com/gin-gonic/gin"
)

func InitCMDBDatabaseUserRouter(Router *gin.RouterGroup) {
	CMDBDatabaseUserRouter := Router.Group("databaseUser").Use(middleware.OperationRecord())
	{
		CMDBDatabaseUserRouter.POST("createDatabaseUser", v1.CreateDatabaseUser)   // 添加CMDB项目
		CMDBDatabaseUserRouter.PUT("updateDatabaseUser", v1.UpdateDatabaseUser)   // 更新CMDB项目
		CMDBDatabaseUserRouter.GET("getDatabaseUserById", v1.GetDatabaseUserById)  // 通过ID获取项目信息
		CMDBDatabaseUserRouter.DELETE("deleteDatabaseUser", v1.DeleteDatabaseUser)  // 通过ID删除项目
		CMDBDatabaseUserRouter.GET("getDatabaseUserList", v1.GetDatabaseUserList) // 获取项目列表

	}
}

