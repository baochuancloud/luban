package router

import (
	v1 "gin-luban-server/api/v1"
	"gin-luban-server/middleware"
	"github.com/gin-gonic/gin"
)

func InitDeployAppJenkinsRouter(Router *gin.RouterGroup) {
	DeployAppJenkinsRouter := Router.Group("jenkins").Use(middleware.OperationRecord())
	{
		DeployAppJenkinsRouter.POST("createDeployAppJenkins", v1.CreateDeployAppJenkins)  //新增
		//DeployAppJenkinsRouter.DELETE("deleteDeployAppJenkins", v1.DeleteDeployAppJenkins)  //新增
		DeployAppJenkinsRouter.PUT("updateDeployAppJenkins", v1.UpdateDeployAppJenkins)   //更新
		DeployAppJenkinsRouter.POST("startJenkinsJobBuild", v1.StartJenkinsJobBuild)  //正常发布构建JOB 触发执行构建
		DeployAppJenkinsRouter.POST("getJenkinsBuildJobLogs", v1.GetJenkinsBuildJobLogs) //查询日志

		DeployAppJenkinsRouter.POST("rollbackJenkinsJobBuild", v1.RollbackJenkinsJobBuild) //回滚

		DeployAppJenkinsRouter.POST("createRollbackJenkinsJob", v1.CreateRollbackJenkinsJob)  // 新增/更新回滚的jenkins job

	}
}
