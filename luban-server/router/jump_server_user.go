package router

import (
	v1 "gin-luban-server/api/v1"
	"gin-luban-server/middleware"
	"github.com/gin-gonic/gin"
)

func InitJumpServerUser(Router *gin.RouterGroup) {
	JumpServerUserRouter := Router.Group("user").Use(middleware.OperationRecord())
	{
		JumpServerUserRouter.POST("createJumpServerUser", v1.CreateJumpServerUser) //新增
		JumpServerUserRouter.GET("getJumpServerUserList",v1.GetJumpServerUserList) //查询接口
		JumpServerUserRouter.GET("getJumpServerUserById",v1.GetJumpServerUserById) //通过ID查询接口
		JumpServerUserRouter.POST("updateJumpServerUser", v1.UpdateJumpServerUser) //更新别名
		JumpServerUserRouter.DELETE("deleteJumpServerUser",v1.DeleteJumpServerUser)  //删除
		JumpServerUserRouter.DELETE("deleteJumpServerUserByIds",v1.DeleteJumpServerUserByIds)  //批量删除
	}
}
