package router

import (
	v1 "gin-luban-server/api/v1"
	"gin-luban-server/middleware"
	"github.com/gin-gonic/gin"
)

func InitDeployAppRouter(Router *gin.RouterGroup) {
	DeployAppRouter := Router.Group("app").Use(middleware.OperationRecord())
	{
		DeployAppRouter.POST("createDeployApp", v1.CreateDeployApp)  //新增
		DeployAppRouter.PUT("updateDeployApp", v1.UpdateDeployApp)   //更新
		DeployAppRouter.DELETE("deleteDeployApp", v1.DeleteDeployApp)//删除
		DeployAppRouter.GET("getDeployAppList", v1.GetDeployAppList) //查询
		DeployAppRouter.POST("copyDeployAppConfigure", v1.CopyAppConfigure)       //拷贝
		//DeployAppRouter.GET("getDeployAppVirtualList",v1.GetDeployAppVirtualList)       //查询应用部署德服务器
		DeployAppRouter.POST("getAppProjectAuthList", v1.GetAppProjectAuthList)                   //获取DeployApp列表
	}
}

