package router

import (
	v1 "gin-luban-server/api/v1"
	"gin-luban-server/middleware"
	"github.com/gin-gonic/gin"
)

func InitJumpServerWS(Router *gin.RouterGroup) {
	JumpServerWsRouter := Router.Group("server")
	{
		JumpServerWsRouter.GET("webWs", v1.WebSshVirtualTerm)
	}
}
func InitJumpServerSftp(Router *gin.RouterGroup) {
	JumpServerSftpRouter := Router.Group("sftp").Use(middleware.OperationRecord())
	{
		JumpServerSftpRouter.POST("getJumpServerSftpLs", v1.GetJumpServerSftpLs)
		JumpServerSftpRouter.POST("deleteJumpServerSftpFile", v1.DeleteJumpServerSftpFile)
		JumpServerSftpRouter.POST("upLoadJumServerSftpFile", v1.UpLoadJumServerSftpFile)
		JumpServerSftpRouter.GET("downLoadJumServerSftpFile", v1.DownLoadJumServerSftpFile)
	}
}

