package router

import (
	v1 "gin-luban-server/api/v1"
	"gin-luban-server/middleware"
	"github.com/gin-gonic/gin"
)

func InitJumpServerFilter(Router *gin.RouterGroup) {
	JumpServerFilterRouter := Router.Group("filter").Use(middleware.OperationRecord())
	{
		JumpServerFilterRouter.POST("createJumpServerCmdFilter", v1.CreateJumpServerCmdFilter) //新增
		JumpServerFilterRouter.PUT("updateJumpServerCmdFilter", v1.UpdateJumpServerCmdFilter) //更新
		JumpServerFilterRouter.GET("getJumpServerCmdFilterList",v1.GetJumpServerCmdFilterList) //查询接口
		JumpServerFilterRouter.DELETE("deleteJumpServerCmdFilter",v1.DeleteJumpServerCmdFilter)  //删除
	}
}
