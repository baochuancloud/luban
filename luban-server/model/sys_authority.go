package model

import (
	"time"
)

type SysAuthority struct {
	CreatedAt       time.Time
	UpdatedAt       time.Time
	DeletedAt       *time.Time     `sql:"index"`
	AuthorityId     string         `json:"authorityId" gorm:"not null;unique;primarykey;comment:角色ID;size:90"`
	AuthorityName   string         `json:"authority_name" gorm:"comment:角色名"`
	Remark          string         `json:"remark"  form:"remark" gorm:"comment:角色备注" `
	Status          *bool          `json:"status" form:"status" gorm:"comment:角色状态"`
	Children        []SysAuthority `json:"children" gorm:"-"`
	SysBaseMenus    []SysBaseMenu  `json:"menus" gorm:"many2many:sys_authority_menus;"`
}
