package model

import "gin-luban-server/global"

type SysDepartment struct {
	global.GVA_MODEL
	DeptCode    string `json:"dept_code" form:"dept_code" gorm:"column:dept_code;type:varchar(50);not null;comment:部门编码" `
	DeptName    string `json:"dept_name" form:"dept_name"  gorm:"column:dept_name;type:varchar(50);not null;comment:部门名称"`
	Status      *bool  `json:"status" form:"status" gorm:"column:status;comment:状态"`
	Remark      string `json:"remark" form:"remark" gorm:"column:remark;type:varchar(100);not null;comment:描述" `
	CompanyCode string `json:"company_code" form:"company_code" gorm:"column:company_code;comment:关联标记"`
	Company     SysCompany `json:"company"  gorm:"foreignKey:CompanyCode;references:CompanyCode;comment:公司code"`
	SysStations []SysStation  `json:"sysStations" gorm:"foreignKey:DeptCode;references:DeptCode;comment:部门code"`
	SysUsers    []SysUser `json:"sysUsers" gorm:"foreignKey:DeptCode;references:DeptCode;comment:公司编码"`
}
