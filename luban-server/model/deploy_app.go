package model

import "gin-luban-server/global"

type DeployAppTree struct {
	Code        string    `json:"code"`        //id
	ParentCode  string    `json:"parent_code"` //父code
	Name        string    `json:"name"`      //菜单名
	Icon        string    `json:"icon"`      //图标路径
	Label       string    `json:"label"`     //标签
	Level       int       `json:"level"`     //级别
	Children    []DeployAppTree  `json:"children" gorm:"-"`
}

type DeployApp struct {
	global.GVA_MODEL
	AppsName      string  `json:"apps_name" form:"apps_name" gorm:"column:apps_name;unique;comment:应用名称"`
	AppsType      string   `json:"apps_type" form:"apps_type" gorm:"column:apps_type;comment:应用类型"`
	GitlabUrl     string  `json:"gitlab_url" form:"gitlab_url" gorm:"column:gitlab_url;comment:gitlab地址"`
	BranchName    string  `json:"branch_name" form:"branch_name" gorm:"column:branch_name;comment:分支名称"`
	ProjectCode   string  `json:"project_code" form:"project_code" gorm:"column:project_code;comment:项目code"`
	Project     CMDBProject `json:"project" gorm:"foreignKey:ProjectCode;references:ProjectCode;comment:项目code"`
	AppConfigures []DeployAppConfigure `json:"AppConfigures" form:"AppConfigures" gorm:"foreignKey:apps_name;references:apps_name;comment:应用名称"`
}

