// 自动生成模板SysDictionaryDetail
package model

import (
	"gin-luban-server/global"
)

// 如果含有time.Time 请自行import time包
type SysDictionaryDetail struct {
	global.GVA_MODEL
	ItemName  string `json:"item_name" form:"item_name" gorm:"column:item_name;comment:展示值"`
	ItemValue string `json:"item_value" form:"item_value" gorm:"column:item_value;comment:字典值"`
	Sort      int    `json:"sort" form:"sort" gorm:"column:sort;comment:排序标记"`
	Status    *bool  `json:"status" form:"status" gorm:"column:status;comment:启用状态"`
	Remark    string `json:"remark" form:"remark" gorm:"type:varchar(100);not null;comment:描述" `
	DictCode  string `json:"dict_code" form:"dict_code" gorm:"column:dict_code;comment:关联标记"`
}
