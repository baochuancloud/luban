package request

import "gin-luban-server/model"



type SearchDeployBuildParams struct {
	model.DeployBuildHistory
	PageInfo
}

type DeploysBuildAppsParams struct {
	AppsCode string   `json:"apps_code" form:"apps_code" validate:"required"`
	ProjectCode string `json:"project_code" form:"project_code" validate:"required"`
	DeployEnv string  `json:"deploy_env" form:"deploy_env" validate:"required"`
}
type ApplyVirtualParams struct {
	AppsCode string   `json:"apps_code" form:"apps_code" validate:"required"`
	DeployEnv string  `json:"deploy_env" form:"deploy_env" validate:"required"`
}
type ApplyDeployMapParams struct {
	ProjectCode string   `json:"project_code" form:"project_code" validate:"required"`
	DeployEnv string  `json:"deploy_env" form:"deploy_env" validate:"required"`
}
