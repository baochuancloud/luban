package request

import "gin-luban-server/model"

type SysDepartmentSearch struct{
	model.SysDepartment
	PageInfo
}
type DepartForm struct {
	model.SysDepartment
}

type GetCompanyCode struct {
	 CompanyCode string `json:"companyCode" validate:"required"`
}
