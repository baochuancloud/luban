package request

import "gin-luban-server/model"

type SysLoginLogsSearch struct {
	model.SysLoginLogs
	PageInfo
}
