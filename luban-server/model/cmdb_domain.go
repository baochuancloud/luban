package model

import (
	"gin-luban-server/global"
	"time"
)

type CMDBDomain struct {
	global.GVA_MODEL
	DomainName   string `json:"domain_name" form:"domain_name" gorm:"column:domain_name;type:varchar(128);not null;comment:域名主机"`
	Protocol     string `json:"protocol" form:"protocol"  gorm:"column:protocol;type:varchar(128);not null;comment:协议 http|https"`
	Status       *bool  `json:"status" form:"status" gorm:"comment:域名状态"`
	DomainIndex  string `json:"domain_index" form:"domain_index" gorm:"column:domain_index;type:varchar(128);not null;comment:首页"`
	Env          string `json:"env" form:"env" gorm:"column:env;type:varchar(128);not null;comment:域名环境"`
	ExternalIp   string `json:"external_ip" form:"external_ip" gorm:"column:external_ip;type:varchar(128);comment:外网IP"`
	ExternalPort string `json:"external_port" form:"external_port" gorm:"column:external_port;type:varchar(255);comment:外网端口"`
	InternalIp   string `json:"internal_ip" form:"internal_ip" gorm:"column:internal_ip;type:varchar(128);comment:内网IP"`
	InternalPort string `json:"internal_port" form:"internal_port" gorm:"column:internal_port;type:varchar(128);comment:内网端口"`
	ICPNumber    string `json:"icp_number" form:"icp_number" gorm:"column:icp_number;type:varchar(128);comment:ICP备案号"`
	GANumber     string `json:"ga_number" form:"ga_number" gorm:"column:ga_number;type:varchar(128);comment:公安备案号"`
	ExpireTime   time.Time `json:"expire_time" form:"expire_time" gorm:"column:expire_time;type:datetime;comment:证书过期时间"`
	ProjectCode  string `json:"project_code" form:"project_code" gorm:"column:project_code;comment:项目代码"`
	Project      CMDBProject `json:"project" gorm:"foreignKey:ProjectCode;references:ProjectCode;comment:项目code"`
}
