package model

import "gin-luban-server/global"

type SysCompany struct {
	global.GVA_MODEL
	CompanyCode string `json:"company_code" form:"company_code" gorm:"type:varchar(50);column:company_code;not null;comment:公司编码"`
	CompanyName string `json:"company_name" form:"company_name" gorm:"type:varchar(50);column:company_name;not null;comment:公司名称"`
	Status      *bool  `json:"status" form:"status" gorm:"column:status;comment:状态"`
	Remark      string `json:"remark" form:"remark" gorm:"type:varchar(100);column:remark;not null;comment:描述" `
	SysDepartments []SysDepartment `json:"sysDepartments" gorm:"foreignKey:CompanyCode;references:CompanyCode;comment:公司编码"`
	SysUsers    []SysUser `json:"sysUsers" gorm:"foreignKey:CompanyCode;references:CompanyCode;comment:公司编码"`
}
