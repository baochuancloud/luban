package model

import (
	"gin-luban-server/global"

)

type CMDBSecurity struct {
	global.GVA_MODEL
	Status      *bool        `json:"status" form:"status" gorm:"column:status;comment:状态;type:tinyint;not null;"`
	Ip          string       `json:"ip" form:"ip" gorm:"column:ip;comment:ip;type:varchar(200);not null;size:200;"`
	Port        string       `json:"port" form:"port" gorm:"column:port;comment:端口;type:int(11);not null;size:11;"`
	Protocol    string       `json:"protocol" form:"protocol" gorm:"column:protocol;comment:网络协议;type:varchar(200);not null;"`
	Remark      string       `json:"remark" form:"remark" gorm:"column:remark;comment:描述;type:varchar(200);not null;size:200;"`
	VirtualCode string       `json:"virtual_code" form:"virtual_code" gorm:"column:virtual_code;comment:关联标记;not null;size:200;"`
	CServer    CmdbServer    `json:"cserver"  gorm:"foreignKey:VirtualCode;references:VirtualCode;comment:服务器的code"`
	NeiPort     string       `json:"neiport" form:"neiport" gorm:"column:neiport;comment:主机端口;type:varchar(200);size:200;"`
	Uput        string        `json:"uput" form:"uput" gorm:"column:uput;comment:流量;type:varchar(200);not null;size:200;"`
}