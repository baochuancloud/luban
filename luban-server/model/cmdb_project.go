package model

import (
	"gin-luban-server/global"
	"time"
)

type CMDBProject struct {
	global.GVA_MODEL
	ProjectCode    string `json:"project_code" form:"project_code" gorm:"column:project_code;type:varchar(50);not null;comment:项目编码"`
	ProjectName    string `json:"project_name" form:"project_name"  gorm:"column:project_name;type:varchar(50);not null;comment:项目名称"`
	ProjectFullName    string `json:"project_fullname" form:"project_fullname"  gorm:"column:project_fullname;type:varchar(50);not null;comment:项目全称"`
	Status          *bool          `json:"status" form:"status" gorm:"comment:项目状态"`
	ProjectManager      string `json:"project_manager" form:"project_manager" gorm:"column:project_manager;type:varchar(128);not null;comment:项目经理"`
	ProjectDevelop     string `json:"project_develop" form:"project_develop" gorm:"column:project_develop;type:varchar(128);not null;comment:项目开发"`
	OperationsMaster     string `json:"operations_master" form:"operations_master" gorm:"column:operations_master;type:varchar(128);not null;comment:项目主运维"`
	OperationsSlave     string `json:"operations_slave" form:"operations_slave" gorm:"column:operations_slave;type:varchar(128);not null;comment:项目备运维"`
	DevelopLanguage     string `json:"develop_language" form:"develop_language" gorm:"column:develop_language;type:varchar(128);not null;comment:开发语言"`
	ProjectAttribute string `json:"project_attribute" form:"project_attribute" gorm:"column:project_attribute;type:varchar(128);not null;comment:项目属性"`
	OnlineTime time.Time `json:"online_time" form:"online_time" gorm:"column:online_time;type:datetime;not null;comment:项目属性"`
	CompanyCode string `json:"company_code" form:"company_code" gorm:"column:company_code;comment:公司代码"`
	Company     SysCompany `json:"company"  gorm:"foreignKey:CompanyCode;references:CompanyCode;comment:公司code"`

}
