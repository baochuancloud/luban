package response

import "gin-luban-server/config"

type SysConfigResponse struct {
	Config config.Server `json:"config"`
}
