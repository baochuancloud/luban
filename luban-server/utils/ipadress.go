package utils

import (
	"encoding/json"
	"gin-luban-server/global"
	"go.uber.org/zap"
	"io/ioutil"
	"net"
	"net/http"
)

//@author: heyibo
//@function: GetLocation
//@description: 根据IP判断地址
//@param: ip string
//@return: string
// 获取外网ip地址
func GetLocation(ip string) string {
	if ip == "127.0.0.1" || ip == "localhost" {
		return "内部IP"
	}
	resp, err := http.Get("https://restapi.amap.com/v3/ip?ip=" + ip + "&key=3fabc36c20379fbb9300c79b19d5d05e")
	if err != nil {
		panic(err)

	}
	defer resp.Body.Close()
	s, err := ioutil.ReadAll(resp.Body)
	m := make(map[string]string)
	err = json.Unmarshal(s, &m)
	if err != nil {
		global.GVA_LOG.Error("Umarshal failed:", zap.Any("err", err))
	}
	if m["province"] == "" {
		return "未知位置"
	}
	return m["province"] + "-" + m["city"]
}

//@author: heyibo
//@function: GetLocaHonst
//@description: 根据IP判断地址
//@param:
//@return: string
// 获取局域网ip地址
func GetLocaHonst() string {
	netInterfaces, err := net.Interfaces()
	if err != nil {
		global.GVA_LOG.Error("net.Interfaces failed, err:", zap.Any("err", err.Error()))
	}
	for i := 0; i < len(netInterfaces); i++ {
		if (netInterfaces[i].Flags & net.FlagUp) != 0 {
			addrs, _ := netInterfaces[i].Addrs()

			for _, address := range addrs {
				if ipnet, ok := address.(*net.IPNet); ok && !ipnet.IP.IsLoopback() {
					if ipnet.IP.To4() != nil {
						return ipnet.IP.String()
					}
				}
			}
		}

	}
	return ""
}
