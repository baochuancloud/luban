package v1

import (
	"gin-luban-server/model/response"
	"gin-luban-server/utils"
	"github.com/gin-gonic/gin"
)

// 系统工具 如dns解析

// @Tags Base
// @Summary dns解析
// @accept application/json
// @Produce application/json
// @Success 200 {string} string "{"success":true,"data":{},"msg":"解析域名成功"}"
// @Router /base/resolverIP [get]
func ResolveIP(c *gin.Context) {
	// 1. 获取参数
	ipType := c.Query("ipType")
	domainName := c.Query("domainName")

	var dnsServer string
	// 后续有必要可以加入到配置文件中
	if ipType == "internal" {
		dnsServer = "10.108.10.1"
	} else {
		dnsServer = "114.114.114.114"
	}

	// 2. 解析IP地址
	dst, err := utils.GetResolveIP(domainName, dnsServer)

	// 3. 返回结果
	if err != nil {
		response.FailWithMessage("dns解析失败", c)
	}
	response.OkWithDetailed(gin.H{"ips": dst}, "解析域名成功!", c)
}
