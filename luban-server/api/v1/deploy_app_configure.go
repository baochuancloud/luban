package v1

import (
	"gin-luban-server/global"
	"gin-luban-server/model/request"
	"gin-luban-server/model/response"
	"gin-luban-server/service"
	"gin-luban-server/utils"
	"github.com/gin-gonic/gin"
	"go.uber.org/zap"
)

// @Tags ApplyConfigure
// @msp 创建项目应用的具体配置
// @Security ApiKeyAuth
// @accept application/json
// @Produce application/json
// @Param data body request.DeployAppConfigureFrom true "body"
// @Success 200 {string} string "{"success":true,"data":{},"msg":"创建成功"}"
// @Router /deploy/dispose/createAppConfigure [post]

func CreateAppConfigure(c *gin.Context)  {
	var R request.DeployAppConfigureFrom
	_ = c.ShouldBindJSON(&R)
	if code,msg := utils.Validate(&R);code != response.SUCCESS_VALIDATE {
		response.FailValidateMessage(msg,c)
		return
	}
	if err := service.CreateDeployAppConfigure(R); err != nil  {
		global.GVA_LOG.Error("创建失败!", zap.Any("err", err))
		response.FailWithMessage("创建失败!"+ err.Error(), c)
		return
	}else {
		response.OkWithMessage("创建成功", c)
	}
}


// @Tags ApplyConfigure
// @Summary 拷贝应用配置
// @Security ApiKeyAuth
// @accept application/json
// @Produce application/json
// @Param data body request.DeployAppCopyRequest true "拷贝应用配置"
// @Success 200 {string} string "{"success":true,"data":{},"msg":"拷贝成功"}"
// @Router /deploy/dispose/CopyAppConfigure [post]
func CopyAppConfigure(c *gin.Context) {
	var copyInfo request.DeployAppCopyRequest
	_ = c.ShouldBindJSON(&copyInfo)
	if code,msg := utils.Validate(&copyInfo);code != response.SUCCESS_VALIDATE {
		response.FailValidateMessage(msg,c)
		return
	}
	if err := service.CopyDeployAppConfigure(uint(copyInfo.Id),copyInfo.DeployEnv); err != nil {
		global.GVA_LOG.Error("拷贝失败!", zap.Any("err", err))
		response.FailWithMessage("拷贝失败"+err.Error(), c)
	} else {
		response.OkWithMessage("拷贝成功", c)
	}
}


// @Tags ApplyConfigure
// @msp 删除项目应用配置
// @Security ApiKeyAuth
// @accept application/json
// @Produce application/json
// @Param data body request.GetById true "body"
// @Success 200 {string} string "{"success":true,"data":{},"msg":"创建成功"}"
// @Router /deploy/dispose/deleteAppConfigure [delete]

func DeleteAppConfigure(c *gin.Context) {
	var R request.GetById
	_ = c.ShouldBindJSON(&R)
	if code,msg := utils.Validate(&R);code != response.SUCCESS_VALIDATE {
		response.FailValidateMessage(msg,c)
		return
	}
	if err := service.DeleteDeployAppConfigure(R.Id); err != nil {
		global.GVA_LOG.Error("删除失败!", zap.Any("err", err))
		response.FailWithMessage("删除失败" + err.Error(), c)
	} else {
		response.OkWithMessage("删除JOB成功", c)
	}
}

// @Tags ApplyConfigure
// @msp 更新项目应用
// @Security ApiKeyAuth
// @accept application/json
// @Produce application/json
// @Param data body request.UpdateDeployAppConfigureFrom true "body"
// @Success 200 {string} string "{"success":true,"data":{},"msg":"创建成功"}"
// @Router /deploy/dispose/updateAppConfigure [put]

func UpdateAppConfigure(c *gin.Context) {
	var configure request.UpdateDeployAppConfigureFrom
	_ = c.ShouldBind(&configure)
	if code,msg := utils.Validate(&configure);code != response.SUCCESS_VALIDATE {
		response.FailValidateMessage(msg,c)
		return
	}
	if err := service.UpdateDeployAppConfigure(configure); err != nil {
		global.GVA_LOG.Error("更新失败!", zap.Any("err", err))
		response.FailWithMessage("更新失败!" + err.Error(), c)
	} else {
		response.OkWithMessage("更新成功", c)
	}
}

// @Tags ApplyConfigure
// @Summary 查找ApplyConfigureList
// @Security ApiKeyAuth
// @accept application/json
// @Produce application/json
// @Param data body request.SearchDeployAppConfigureParams true "页码, 每页大小, 搜索条件"
// @Success 200 {string} string "{"success":true,"data":{},"msg":"获取成功"}"
// @Router /deploy/dispose/GetApplyConfigureList [get]
func GetAppConfigureList(c *gin.Context) {
	var pageInfo request.SearchDeployAppConfigureParams
	_ = c.ShouldBind(&pageInfo)
	if code,msg := utils.Validate(&pageInfo );code != response.SUCCESS_VALIDATE {
		response.FailValidateMessage(msg,c)
		return
	}
	if err, list, total := service.GetDeployAppConfigureInfoList(pageInfo); err != nil {
		global.GVA_LOG.Error("获取失败!", zap.Any("err", err))
		response.FailWithMessage("获取失败", c)
	} else {
		response.OkWithDetailed(response.PageResult{
			List:     list,
			Total:    total,
			Page:     pageInfo.Page,
			PageSize: pageInfo.PageSize,
		}, "获取成功", c)
	}
}