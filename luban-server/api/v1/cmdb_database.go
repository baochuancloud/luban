package v1

import (
	"gin-luban-server/global"
	"gin-luban-server/model"
	"gin-luban-server/model/request"
	"gin-luban-server/model/response"
	"gin-luban-server/service"
	"gin-luban-server/utils"
	"github.com/gin-gonic/gin"
	"go.uber.org/zap"
	"strings"
)

// @Tags CMDBDatabase
// @msp 创建数据库信息
// @Security ApiKeyAuth
// @accept application/json
// @Produce application/json
// @Param data body request.DatabaseForm true "body"
// @Success 200 {string} string "{"success":true,"data":{},"msg":"创建成功"}"
// @Router /cmdb/database/createDatabase [post]
func CreateDatabase(c *gin.Context) {
	var R request.DatabaseForm
	_ = c.ShouldBindJSON(&R)
	if code,msg := utils.Validate(&R);code != response.SUCCESS_VALIDATE {
		response.FailValidateMessage(msg,c)
		return
	}
    database := model.CMDBDatabase{ClusterName: R.ClusterName,DatabaseType: R.DatabaseType,VIP: R.VIP,Port: R.Port,EnvName: R.EnvName,CompanyCode: R.CompanyCode,ProjectCode:R.ProjectCode,IdcLocation: R.IdcLocation,
    	IpAddress: R.IpAddress,DnsName: R.DnsName,InstanceName: R.InstanceName,DeployPath: R.DeployPath,DatabaseSize: R.DatabaseSize,
    }
	if err := service.CreateDatabase(database); err != nil {
		global.GVA_LOG.Error("创建失败!", zap.Any("err", err))
		response.FailWithMessage("创建失败" + err.Error(), c)
	} else {
		response.OkWithMessage("创建成功", c)
	}
}

// @Tags CMDBDatabase
// @Summary 删除数据库信息
// @Security ApiKeyAuth
// @accept application/json
// @Produce application/json
// @Param data body request.GetById true "body"
// @Success 200 {string} string "{"success":true,"data":{},"msg":"删除成功"}"
// @Router /cmdb/database/deleteDatabase [delete]
func DeleteDatabase(c *gin.Context) {
	var Ids request.GetById
	_ = c.ShouldBindJSON(&Ids)
	if code,msg := utils.Validate(&Ids );code != response.SUCCESS_VALIDATE {
		response.FailValidateMessage(msg,c)
		return
	}
	if err := service.DeleteDatabase(Ids.Id); err != nil {
		global.GVA_LOG.Error("删除失败!", zap.Any("err", err))
		response.FailWithMessage("删除失败!有账户正在使用", c)
	} else {
		response.OkWithMessage("删除成功", c)
	}
}


// @Tags CMDBDatabase
// @Summary 更新数据库信息
// @Security ApiKeyAuth
// @accept application/json
// @Produce application/json
// @Param data body model.CMDBDatabase true "CMDBDatabase模型"
// @Success 200 {string} string "{"success":true,"data":{},"msg":"更新成功"}"
// @Router /cmdb/database/updateDatabase [put]
func UpdateDatabase(c *gin.Context) {
	var database model.CMDBDatabase
	_ = c.ShouldBind(&database)
	if err := service.UpdateDatabase(database); err != nil {
		global.GVA_LOG.Error("更新失败!", zap.Any("err", err))
		response.FailWithMessage("更新失败!" + err.Error(), c)
	} else {
		response.OkWithMessage("更新成功", c)
	}
}

// @Tags CMDBDatabase
// @Summary 查找ProjectList
// @Security ApiKeyAuth
// @accept application/json
// @Produce application/json
// @Param data body request.SearchDatabaseParams true "页码, 每页大小, 搜索条件"
// @Success 200 {string} string "{"success":true,"data":{},"msg":"获取成功"}"
// @Router /cmdb/database/getDatabaseList [get]
func GetDatabaseList(c *gin.Context) {
	var pageInfo request.SearchDatabaseParams
	_ = c.ShouldBind(&pageInfo)
	if code,msg := utils.Validate(&pageInfo );code != response.SUCCESS_VALIDATE {
		response.FailValidateMessage(msg,c)
		return
	}
	if err, list, total := service.GetDatabaseInfoList(pageInfo); err != nil {
		global.GVA_LOG.Error("获取失败!", zap.Any("err", err))
		response.FailWithMessage("获取失败", c)
	} else {
		response.OkWithDetailed(response.PageResult{
			List:     list,
			Total:    total,
			Page:     pageInfo.Page,
			PageSize: pageInfo.PageSize,
		}, "获取成功", c)
	}
}



// @Tags CMDBDatabase
// @Summary 通过ID查找CMDBDatabase
// @Security ApiKeyAuth
// @accept application/json
// @Produce application/json
// @Param data body request.GetById true "根据id获取"
// @Success 200 {string} string "{"success":true,"data":{},"msg":"获取成功"}"
// @Router /cmdb/database/getDatabaseById [post]
func GetDatabaseById(c *gin.Context) {
	var reqId request.GetById
	_ = c.ShouldBind(&reqId)
	if code, msg := utils.Validate(&reqId); code != response.SUCCESS_VALIDATE {
		response.FailValidateMessage(msg, c)
		return
	}
	if err, results := service.FindDatabaseById(reqId.Id); err != nil {
		global.GVA_LOG.Error("获取数据失败!", zap.Any("err", err))
		response.FailWithMessage("获取数据失败", c)
	} else {
		response.OkWithDetailed(gin.H{"databaseList": results}, "获取数据成功!", c)
	}

}


// @Tags excel
// @Summary 导出Excel
// @Security ApiKeyAuth
// @accept application/json
// @Produce  application/octet-stream
// @Param data body request.ExcelDatabase true "导出Excel文件信息"
// @Success 200
// @Router /excel/exportExcel [get]
func DatabaseExportExcel(c *gin.Context) {
	var R request.ExcelDatabase
	// 绑定查询参数
	_ = c.ShouldBindQuery(&R)
	// 自动追加excel后缀名
	if ! strings.HasSuffix(R.FileName, ".xlsx") {
		R.FileName = R.FileName + ".xlsx"
	}
	filePath := global.GVA_CONFIG.Excel.Dir + R.FileName
	err := service.ParseDatabaseInfoList2Excel(R.CMDBDatabaseUser, filePath)
	if err != nil {
		global.GVA_LOG.Error("转换Excel失败!", zap.Any("err", err))
		response.FailWithMessage("转换Excel失败", c)
		return
	}
	c.Writer.Header().Add("success", "true")
	c.File(filePath)
}

