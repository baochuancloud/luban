package v1

import (
	"gin-luban-server/global"
	"gin-luban-server/model"
	"gin-luban-server/model/request"
	"gin-luban-server/model/response"
	"gin-luban-server/service"
	"gin-luban-server/utils"
	"github.com/gin-gonic/gin"
	"go.uber.org/zap"
	"strings"
)


// @Tags CMDBProject
// @msp 创建项目
// @Security ApiKeyAuth
// @accept application/json
// @Produce application/json
// @Param data body model.CMDBProject true "body"
// @Success 200 {string} string "{"success":true,"data":{},"msg":"创建成功"}"
// @Router /cmdb/project/createProject [post]
func CreateProject(c *gin.Context) {
	var R model.CMDBProject
	_ = c.ShouldBindJSON(&R)
	if err := utils.Verify(R, utils.ProjectVerify); err != nil {
		response.FailWithMessage(err.Error(), c)
		return
	}

	if err := service.CreateProject(R); err != nil {
		global.GVA_LOG.Error("创建失败!", zap.Any("err", err))
		response.FailWithMessage("创建失败" + err.Error(), c)
	} else {
		response.OkWithMessage("创建成功", c)
	}
}

// @Tags CMDBProject
// @Summary 删除项目
// @Security ApiKeyAuth
// @accept application/json
// @Produce application/json
// @Param data body request.GetById true "根据ID删除"
// @Success 200 {string} string "{"success":true,"data":{},"msg":"删除成功"}"
// @Router /cmdb/project/deleteProject [delete]
func DeleteProject(c *gin.Context) {
	var project request.GetById
	_ = c.ShouldBindJSON(&project)
	if code,msg := utils.Validate(&project );code != response.SUCCESS_VALIDATE {
		response.FailValidateMessage(msg,c)
		return
	}
	if err := service.DeleteProject(project.Id); err != nil {
		global.GVA_LOG.Error("删除失败!", zap.Any("err", err))
		response.FailWithMessage("删除失败!" + err.Error(), c)
	} else {
		response.OkWithMessage("删除成功", c)
	}
}


// @Tags CMDBProject
// @Summary 更新Project
// @Security ApiKeyAuth
// @accept application/json
// @Produce application/json
// @Param data body model.CMDBProject true "CMDBProject模型"
// @Success 200 {string} string "{"success":true,"data":{},"msg":"更新成功"}"
// @Router /cmdb/project/updateProject [put]
func UpdateProject(c *gin.Context) {
	var R model.CMDBProject
	_ = c.ShouldBindJSON(&R)
	if err := utils.Verify(R, utils.ProjectVerify); err != nil {
		response.FailWithMessage(err.Error(), c)
		return
	}
	if err := service.UpdateProject(R); err != nil {
		global.GVA_LOG.Error("更新失败!", zap.Any("err", err))
		response.FailWithMessage("更新失败!" + err.Error(), c)
	} else {
		response.OkWithMessage("更新成功", c)
	}
}

// @Tags CMDBProject
// @Summary 查找ProjectList
// @Security ApiKeyAuth
// @accept application/json
// @Produce application/json
// @Param data body request.SearchCMDBProjectParams true "页码, 每页大小, 搜索条件"
// @Success 200 {string} string "{"success":true,"data":{},"msg":"获取成功"}"
// @Router /cmdb/project/getProjectList [get]
func GetCMDBProjectList(c *gin.Context) {
	var pageInfo request.SearchCMDBProjectParams
	_ = c.ShouldBind(&pageInfo)
	if err := utils.Verify(pageInfo.PageInfo, utils.PageInfoVerify); err != nil {
		response.FailWithMessage(err.Error(), c)
		return
	}
	if err, list, total := service.GetCMDBProjectInfoList(pageInfo.CMDBProject, pageInfo.PageInfo, pageInfo.Desc); err != nil {
		global.GVA_LOG.Error("获取失败!", zap.Any("err", err))
		response.FailWithMessage("获取失败", c)
	} else {
		response.OkWithDetailed(response.PageResult{
			List:     list,
			Total:    total,
			Page:     pageInfo.Page,
			PageSize: pageInfo.PageSize,
		}, "获取成功", c)
	}
}



// @Tags CMDBProject
// @Summary 通过ID查找Project
// @Security ApiKeyAuth
// @accept application/json
// @Produce application/json
// @Param data body request.GetById true "根据id获取project"
// @Success 200 {string} string "{"success":true,"data":{},"msg":"获取成功"}"
// @Router /cmdb/project/getProjectById [post]
func GetProjectById(c *gin.Context) {
	var reqId request.GetById
	_ = c.ShouldBindJSON(&reqId)
	if code, msg := utils.Validate(&reqId); code != response.SUCCESS_VALIDATE {
		response.FailValidateMessage(msg, c)
		return
	}
	if err, results := service.FindProjectById(reqId.Id); err != nil {
		global.GVA_LOG.Error("获取数据失败!", zap.Any("err", err))
		response.FailWithMessage("获取数据失败", c)
	} else {
		response.OkWithDetailed(response.CMDBProjectResponse{Project: results}, "获取数据成功!", c)
	}

}


// @Tags excel
// @Summary 导出Excel
// @Security ApiKeyAuth
// @accept application/json
// @Produce  application/octet-stream
// @Param data body request.ExcelProject true "导出Excel文件信息"
// @Success 200
// @Router /excel/exportExcel [get]
func ProjectExportExcel(c *gin.Context) {
	var R request.ExcelProject
	// 绑定查询参数
	_ = c.ShouldBindQuery(&R)
	// 自动追加excel后缀名
	if ! strings.HasSuffix(R.FileName, ".xlsx") {
		R.FileName = R.FileName + ".xlsx"
	}
	filePath := global.GVA_CONFIG.Excel.Dir + R.FileName
	err := service.ParseProjectInfoList2Excel(R.CMDBProject, filePath)
	if err != nil {
		global.GVA_LOG.Error("转换Excel失败!", zap.Any("err", err))
		response.FailWithMessage("转换Excel失败", c)
		return
	}
	c.Writer.Header().Add("success", "true")
	c.File(filePath)
}



