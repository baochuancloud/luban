package v1

import (
	"errors"
	"fmt"
	"gin-luban-server/global"
	"gin-luban-server/model/request"
	"gin-luban-server/model/response"
	"gin-luban-server/service"
	"gin-luban-server/utils"
	"github.com/gin-gonic/gin"
	"go.uber.org/zap"
	"net/http"
	"strconv"
)

// @Tags GetJumpServerSftpLs
// @msp 查看文件列表
// @Security ApiKeyAuth
// @accept application/json
// @Produce application/json
// @Param data body request.SftpLsFrom true "body"
// @Success 200 {string} string "{"success":true,"data":{},"msg":"创建成功"}"
// @Router /jump/sftp/getJumpServerSftpLs
func GetJumpServerSftpLs (c *gin.Context)  {
	var R request.SftpLsFrom
	_ = c.ShouldBindJSON(&R)
	if code,msg := utils.Validate(&R);code != response.SUCCESS_VALIDATE {
		response.FailValidateMessage(msg,c)
		return
	}
	if list, err := service.GetSftpLs(R.ID,R.Uuid,R.DirPath); err != nil {
		global.GVA_LOG.Error("获取数据失败!", zap.Any("err", err))
		response.FailWithMessage("获取数据失败" + err.Error(), c)
	} else {
		response.OkWithDetailed(gin.H{"list": list}, "获取数据成功!", c)
	}
}

// @Tags DeleteJumpServerSftpFile
// @msp 查看文件列表
// @Security ApiKeyAuth
// @accept application/json
// @Produce application/json
// @Param data body request.SftpDeleteFileFrom true "body"
// @Success 200 {string} string "{"success":true,"data":{},"msg":"创建成功"}"
// @Router /jump/sftp/deleteJumpServerSftpFile
func DeleteJumpServerSftpFile(c *gin.Context) {
	var R request.SftpDeleteFileFrom
	_ = c.ShouldBind(&R)
	if code,msg := utils.Validate(&R);code != response.SUCCESS_VALIDATE {
		response.FailValidateMessage(msg,c)
		return
	}
	if err := service.DeleteSftpFile(R.ID,R.Uuid,R.FilePath); err != nil {
		global.GVA_LOG.Error("删除失败!", zap.Any("err", err))
		response.FailWithMessage("删除失败！" + err.Error(), c)
	} else {
		response.OkWithMessage("删除成功", c)
	}
}

// @Tags UpLoadJumServerSftpFile
// @msp 查看文件列表
// @Security ApiKeyAuth
// @accept application/json
// @Produce application/json
// @Param data body request.SftpUploadFileFrom true "body"
// @Success 200 {string} string "{"success":true,"data":{},"msg":"创建成功"}"
// @Router /jump/sftp/upLoadJumServerSftpFile
func UpLoadJumServerSftpFile(c *gin.Context)  {
	id := c.DefaultQuery("id", "")
	uuid :=c.DefaultQuery("uuid", "")
	desDir :=c.DefaultQuery("desDir", "")
	if  id == "" || uuid == "" || desDir == ""  {
		err :=errors.New("参数为空")
		global.GVA_LOG.Error("上传失败!", zap.Any("err", err))
		response.FailWithMessage("上传失败" + err.Error(), c)
	}
	_, header, err := c.Request.FormFile("file")
	if err != nil {
		global.GVA_LOG.Error("接收文件失败!", zap.Any("err", err))
		response.FailWithMessage("接收文件失败", c)
	}
	ID,_:=strconv.ParseFloat(id,64)
	if err := service.UpLoadFileSftp(ID,uuid,desDir,header); err != nil {
		global.GVA_LOG.Error("上传失败!", zap.Any("err", err))
		response.FailWithMessage("上传失败！", c)
	} else {
		response.OkWithMessage("上传成功", c)
	}
}

// @Tags DownLoadJumServerSftpFile
// @msp 下载文件
// @Security ApiKeyAuth
// @accept application/json
// @Produce application/json
// @Param data body request.SftpUploadFileFrom true "body"
// @Success 200 {string} string "{"success":true,"data":{},"msg":"创建成功"}"
// @Router /jump/sftp/downLoadJumServerSftpFile

func DownLoadJumServerSftpFile(c *gin.Context)  {
	id := c.DefaultQuery("id", "")
	uuid :=c.DefaultQuery("uuid", "")
	filePath :=c.DefaultQuery("filePath", "")
	fileName :=c.DefaultQuery("fileName", "")
	if  id == "" || uuid == "" || filePath == "" || fileName == ""  {
		err :=errors.New("参数为空")
		global.GVA_LOG.Error("下载失败!", zap.Any("err", err))
		response.FailWithMessage("下载失败" + err.Error(), c)
	}
	ID,_:=strconv.ParseFloat(id,64)
	clientInfo,err := service.GetSftpClientInfo(ID,uuid)
	if err != nil {
		response.FailWithMessage("获取用户信息失败！", c)
	}
	sshClient,err :=service.NewSshClient(clientInfo)
	if err != nil {
		response.FailWithMessage("生成ssh客户端失败！", c)
	}
	sftpClient,err :=service.NewSftpClient(sshClient)
	if err != nil {
		response.FailWithMessage("生成sftp客户端失败！", c)
	}
	fileSize, _:= sftpClient.Stat(filePath)
	file, err := sftpClient.Open(filePath)
	if err != nil {
		response.FailWithMessage("sftp客户端打开文件失败！", c)
	}
	defer sftpClient.Close()
	extraHeaders := map[string]string{
		"Content-Disposition": fmt.Sprintf(`attachment; filename="%s"`,fileName),
	}
	c.DataFromReader(http.StatusOK, fileSize.Size(), "application/octet-stream", file, extraHeaders)
	return
}