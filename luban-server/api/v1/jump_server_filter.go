package v1

import (
	"gin-luban-server/global"
	"gin-luban-server/model"
	"gin-luban-server/model/request"
	"gin-luban-server/model/response"
	"gin-luban-server/service"
	"gin-luban-server/utils"
	"github.com/gin-gonic/gin"
	"go.uber.org/zap"
)
// @Tags JumpServerCmdFilter
// @Summary 添加堡垒机服务器
// @Security ApiKeyAuth
// @accept application/json
// @Produce application/json
// @Param data body request.JumpServerCmdFilterFrom true "空"
// @Success 200 {string} string "{"success":true,"data":{},"msg":"获取成功"}"
// @Router /jump/filter/createJumpServerUser [post]
func CreateJumpServerCmdFilter(c *gin.Context) {
	var R request.JumpServerCmdFilterFrom
	_ = c.ShouldBind(&R)
	if code,msg := utils.Validate(&R);code != response.SUCCESS_VALIDATE {
		response.FailValidateMessage(msg,c)
		return
	}
	filter :=model.JumpServerCmdFilter{Command: R.Command,Msg: R.Msg,Enable: R.Enable}
	if err := service.CreateJumpServerCmdFilter(filter); err != nil {
		global.GVA_LOG.Error("创建失败!", zap.Any("err", err))
		response.FailWithMessage("创建失败" + err.Error(), c)
	} else {
		response.OkWithMessage("创建成功", c)
	}
}

// @Tags JumpServerCmdFilter
// @msp 删除堡垒机服务器
// @Security ApiKeyAuth
// @accept application/json
// @Produce application/json
// @Param data body model.JumpServerCmdFilter true "body"
// @Success 200 {string} string "{"success":true,"data":{},"msg":"创建成功"}"
// @Router /jump/filter/deleteJumpServerCmdFilter [delete]

func DeleteJumpServerCmdFilter(c *gin.Context) {
	var Ids request.GetById
	_ = c.ShouldBindJSON(&Ids)
	if code,msg := utils.Validate(&Ids );code != response.SUCCESS_VALIDATE {
		response.FailValidateMessage(msg,c)
		return
	}
	if err := service.DeleteJumpServerCmdFilter(Ids.Id); err != nil {
		global.GVA_LOG.Error("删除失败!", zap.Any("err", err))
		response.FailWithMessage("删除失败!", c)
	} else {
		response.OkWithMessage("删除成功", c)
	}
}

// @Tags JumpServerCmdFilter
// @msp 更新项目应用
// @Security ApiKeyAuth
// @accept application/json
// @Produce application/json
// @Param data body model.JumpServerCmdFilter true "body"
// @Success 200 {string} string "{"success":true,"data":{},"msg":"创建成功"}"
// @Router /jump/filter/updateJumpServerCmdFilter [put]

func UpdateJumpServerCmdFilter(c *gin.Context) {
	var apply model.JumpServerCmdFilter
	_ = c.ShouldBind(&apply)
	if err := service.UpdateJumpServerCmdFilter(apply); err != nil {
		global.GVA_LOG.Error("更新失败!", zap.Any("err", err))
		response.FailWithMessage("更新失败", c)
	} else {
		response.OkWithMessage("更新成功", c)
	}
}
// @Tags JumpServerCmdFilter
// @Summary 查找JumpServerCmdFilterList
// @Security ApiKeyAuth
// @accept application/json
// @Produce application/json
// @Param data body request.SearchJumpServerCmdFilterParams true "页码, 每页大小, 搜索条件"
// @Success 200 {string} string "{"success":true,"data":{},"msg":"获取成功"}"
// @Router  /jump/filter/getJumpServerCmdFilterList [get]
func GetJumpServerCmdFilterList(c *gin.Context) {
	var pageInfo request.SearchJumpServerCmdFilterParams
	_ = c.ShouldBind(&pageInfo)
	if code,msg := utils.Validate(&pageInfo );code != response.SUCCESS_VALIDATE {
		response.FailValidateMessage(msg,c)
		return
	}
	if err, list, total := service.GetJumpServerCmdFilterInfoList(pageInfo); err != nil {
		global.GVA_LOG.Error("获取失败!", zap.Any("err", err))
		response.FailWithMessage("获取失败", c)
	} else {
		response.OkWithDetailed(response.PageResult{
			List:     list,
			Total:    total,
			Page:     pageInfo.Page,
			PageSize: pageInfo.PageSize,
		}, "获取成功", c)
	}
}

