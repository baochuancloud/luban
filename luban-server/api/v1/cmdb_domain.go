package v1

import (
	"gin-luban-server/global"
	"gin-luban-server/model"
	"gin-luban-server/model/request"
	"gin-luban-server/model/response"
	"gin-luban-server/service"
	"gin-luban-server/utils"
	"github.com/gin-gonic/gin"
	"go.uber.org/zap"
	"strings"
)


// @Tags CMDBDomain
// @msp 创建域名信息
// @Security ApiKeyAuth
// @accept application/json
// @Produce application/json
// @Param data body model.CMDBDomain true "body"
// @Success 200 {string} string "{"success":true,"data":{},"msg":"创建成功"}"
// @Router /cmdb/domain/createDomain [post]
func CreateDomain(c *gin.Context) {
	var R model.CMDBDomain
	_ = c.ShouldBindJSON(&R)
	if err := utils.Verify(R, utils.DomainVerify); err != nil {
		response.FailWithMessage(err.Error(), c)
		return
	}
	if err := service.CreateDomain(R); err != nil {
		global.GVA_LOG.Error("创建失败!", zap.Any("err", err))
		response.FailWithMessage("创建失败" + err.Error(), c)
	} else {
		response.OkWithMessage("创建成功", c)
	}
}


// @Tags CMDBDomain
// @Summary 删除域名信息
// @Security ApiKeyAuth
// @accept application/json
// @Produce application/json
// @Param data body model.CMDBDomain true "body"
// @Success 200 {string} string "{"success":true,"data":{},"msg":"删除成功"}"
// @Router /cmdb/domain/deleteDomain [delete]
func DeleteDomain(c *gin.Context) {
	var domain request.GetById
	_ = c.ShouldBindJSON(&domain)
	if code,msg := utils.Validate(&domain );code != response.SUCCESS_VALIDATE {
		response.FailValidateMessage(msg,c)
		return
	}
	if err := service.DeleteDomain(domain.Id); err != nil {
		global.GVA_LOG.Error("删除失败!", zap.Any("err", err))
		response.FailWithMessage("删除失败!" + err.Error(), c)
	} else {
		response.OkWithMessage("删除成功", c)
	}
}

// @Tags CMDBDomain
// @Summary 更新Domain
// @Security ApiKeyAuth
// @accept application/json
// @Produce application/json
// @Param data body model.CMDBDomain true "CMDBDomain模型"
// @Success 200 {string} string "{"success":true,"data":{},"msg":"更新成功"}"
// @Router /cmdb/domain/updateDomain [put]
func UpdateDomain(c *gin.Context) {
	var R model.CMDBDomain
	_ = c.ShouldBindJSON(&R)
	if err := utils.Verify(R, utils.DomainVerify); err != nil {
		response.FailWithMessage(err.Error(), c)
		return
	}
	if err := service.UpdateDomain(R); err != nil {
		global.GVA_LOG.Error("更新失败!", zap.Any("err", err))
		response.FailWithMessage("更新失败!" + err.Error(), c)
	} else {
		response.OkWithMessage("更新成功", c)
	}
}

// @Tags CMDBDomain
// @Summary 查找DomainList
// @Security ApiKeyAuth
// @accept application/json
// @Produce application/json
// @Param data body request.SearchCMDBDomainParams true "页码, 每页大小, 搜索条件"
// @Success 200 {string} string "{"success":true,"data":{},"msg":"获取成功"}"
// @Router /cmdb/project/getDomainList [get]
func GetCMDBDomainList(c *gin.Context) {
	var pageInfo request.SearchCMDBDomainParams
	_ = c.ShouldBind(&pageInfo)
	if err := utils.Verify(pageInfo.PageInfo, utils.PageInfoVerify); err != nil {
		response.FailWithMessage(err.Error(), c)
		return
	}
	if err, list, total := service.GetCMDBDomainInfoList(pageInfo.CMDBDomain, pageInfo.PageInfo, pageInfo.Desc); err != nil {
		global.GVA_LOG.Error("获取失败!", zap.Any("err", err))
		response.FailWithMessage("获取失败", c)
	} else {
		response.OkWithDetailed(response.PageResult{
			List:     list,
			Total:    total,
			Page:     pageInfo.Page,
			PageSize: pageInfo.PageSize,
		}, "获取成功", c)
	}
}


// @Tags CMDBDomain
// @Summary 通过ID查找Domain
// @Security ApiKeyAuth
// @accept application/json
// @Produce application/json
// @Param data body request.GetById true "根据id获取domain"
// @Success 200 {string} string "{"success":true,"data":{},"msg":"获取成功"}"
// @Router /cmdb/domain/getDomainById [post]
func GetDomainById(c *gin.Context) {
	var reqId request.GetById
	_ = c.ShouldBindJSON(&reqId)
	if code, msg := utils.Validate(&reqId); code != response.SUCCESS_VALIDATE {
		response.FailValidateMessage(msg, c)
		return
	}
	if err, results := service.FindDomainById(reqId.Id); err != nil {
		global.GVA_LOG.Error("获取数据失败!", zap.Any("err", err))
		response.FailWithMessage("获取数据失败", c)
	} else {
		response.OkWithDetailed(response.CMDBDomainResponse{Domain: results}, "获取数据成功!", c)
	}

}


// @Tags CMDBDomain
// @Summary 导出Excel
// @Security ApiKeyAuth
// @accept application/json
// @Produce  application/octet-stream
// @Param data body request.ExcelDomain true "导出Excel文件信息"
// @Success 200
// @Router /excel/exportExcel [get]
func DomainExportExcel(c *gin.Context) {
	var R request.ExcelDomain
	// 绑定查询参数
	_ = c.ShouldBindQuery(&R)
	// 自动追加excel后缀名
	if ! strings.HasSuffix(R.FileName, ".xlsx") {
		R.FileName = R.FileName + ".xlsx"
	}
	filePath := global.GVA_CONFIG.Excel.Dir + R.FileName
	err := service.ParseDomainInfoList2Excel(R.CMDBDomain, filePath)
	if err != nil {
		global.GVA_LOG.Error("转换Excel失败!", zap.Any("err", err))
		response.FailWithMessage("转换Excel失败", c)
		return
	}
	c.Writer.Header().Add("success", "true")
	c.File(filePath)
}