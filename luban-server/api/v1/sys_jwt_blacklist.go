package v1

import (
	"gin-luban-server/global"
	"gin-luban-server/model"
	"gin-luban-server/model/request"
	"gin-luban-server/model/response"
	"gin-luban-server/service"
	"github.com/gin-gonic/gin"
	"go.uber.org/zap"
)

// @Tags Jwt
// @Summary jwt加入黑名单
// @Security ApiKeyAuth
// @accept application/json
// @Produce application/json
// @Success 200 {string} string "{"success":true,"data":{},"msg":"拉黑成功"}"
// @Router /jwt/jsonInBlacklist [post]
func JsonInBlacklist(c *gin.Context) {
	token := c.Request.Header.Get("x-token")
	jwt := model.JwtBlacklist{Jwt: token}
	if err := service.JsonInBlacklist(jwt); err != nil {
		global.GVA_LOG.Error("jwt作废失败!", zap.Any("err", err))
		msg := "退出失败"
		status := false
		if claims, ok := c.Get("claims"); ok {
			waitUse := claims.(*request.CustomClaims)
			CreateLoginLogs(c, &status, msg, waitUse.Username, waitUse.NickName)
		}
		response.FailWithMessage("jwt作废失败", c)
	} else {
		msg := "退出成功"
		status := true
		if claims, ok := c.Get("claims"); ok {
			waitUse := claims.(*request.CustomClaims)
			CreateLoginLogs(c, &status, msg, waitUse.Username, waitUse.NickName)
		}
		response.OkWithMessage("jwt作废成功", c)
	}
}
