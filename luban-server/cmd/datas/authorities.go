package datas

import (
	"github.com/gookit/color"
	"os"
	"time"

	"gin-luban-server/model"
	"gorm.io/gorm"
)

func InitSysAuthority(db *gorm.DB) {
	status := new(bool)
	*status = true
	var Authorities = []model.SysAuthority{
		{CreatedAt: time.Now(), UpdatedAt: time.Now(), AuthorityId: "admin", AuthorityName: "管理员", Status: status,Remark: "超级管理员"},
	}
	if err := db.Transaction(func(tx *gorm.DB) error {
		if tx.Where("authority_id IN ? ", []string{"admin"}).Find(&[]model.SysAuthority{}).RowsAffected == 1 {
			color.Danger.Println("sys_authorities表的初始数据已存在!")
			return nil
		}
		if err := tx.Create(&Authorities).Error; err != nil { // 遇到错误时回滚事务
			return err
		}
		return nil
	}); err != nil {
		color.Warn.Printf("[Mysql]--> sys_authorities 表的初始数据失败,err: %v\n", err)
		os.Exit(0)
	}
}
