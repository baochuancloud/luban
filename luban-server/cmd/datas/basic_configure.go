package datas

import (
	"gin-luban-server/global"
	"gin-luban-server/model"
	"github.com/gookit/color"
	"gorm.io/gorm"
	"os"
	"time"
)

func InitSysBasicConfigure(db *gorm.DB) {
	status := new(bool)
	*status = true
	Basic := []model.SysBasicConfigure{
		{GVA_MODEL: global.GVA_MODEL{ID: 1, CreatedAt: time.Now(), UpdatedAt: time.Now()},BasicUser: "deploy",SshType: "password",BasicPasswd: "it-msp@anji-allways.com",Purpose: "gitlab",BaseUrl: "http://gitlab.anji-allways.com/",Remark: "gitlab",Status: status},
		{GVA_MODEL: global.GVA_MODEL{ID: 2, CreatedAt: time.Now(), UpdatedAt: time.Now()},BasicUser: "admin",SshType: "password",BasicPasswd: "admin@1234",Purpose: "jenkins",BaseUrl: "http://10.108.12.152:8080/",ServerIdc: "Local",Remark: "开发环境jenkins",Status: status},
		{GVA_MODEL: global.GVA_MODEL{ID: 3, CreatedAt: time.Now(), UpdatedAt: time.Now()},BasicUser: "appuser",SshType: "password",BasicPasswd: "p@ss1234",Purpose: "os_appuser",Remark: "appuser账号",Status: status},
		{GVA_MODEL: global.GVA_MODEL{ID: 4, CreatedAt: time.Now(), UpdatedAt: time.Now()},BasicUser: "devuser",SshType: "password",BasicPasswd: "p@ss1234",Purpose: "os_devuser",Remark: "devuser账号",Status: status},
		{GVA_MODEL: global.GVA_MODEL{ID: 5, CreatedAt: time.Now(), UpdatedAt: time.Now()},BasicUser: "appuser",SshType: "password",BasicPasswd: "p@ss1234",Purpose: "ansible",ServerIdc: "Local",Remark: "本地ansile服务器",Status: status},
	}
	if err := db.Transaction(func(tx *gorm.DB) error {
		if tx.Where("id IN ?", []int{1,5}).Find(&[]model.SysUser{}).RowsAffected == 2 {
			color.Danger.Println("sys_basic_configure表的初始数据已存在!")
			return nil
		}
		if err := tx.Create(&Basic).Error; err != nil { // 遇到错误时回滚事务
			return err
		}
		return nil
	}); err != nil {
		color.Warn.Printf("[Mysql]--> sys_basic_configure 表的初始数据失败,err: %v\n", err)
		os.Exit(0)
	}
}

