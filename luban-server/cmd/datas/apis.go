package datas

import (
	"gin-luban-server/global"
	"gin-luban-server/model"
	"github.com/gookit/color"
	"os"
	"time"

	"gorm.io/gorm"
)

var Apis = []model.SysApi{
	{global.GVA_MODEL{ID: 1, CreatedAt: time.Now(), UpdatedAt: time.Now()}, "/base/login", "用户登录", "base", "POST"},
	{global.GVA_MODEL{ID: 2, CreatedAt: time.Now(), UpdatedAt: time.Now()}, "/user/register", "用户注册", "user", "POST"},
	{global.GVA_MODEL{ID: 3, CreatedAt: time.Now(), UpdatedAt: time.Now()}, "/api/createApi", "创建api", "api", "POST"},
	{global.GVA_MODEL{ID: 4, CreatedAt: time.Now(), UpdatedAt: time.Now()}, "/api/getApiList", "获取api列表", "api", "POST"},
	{global.GVA_MODEL{ID: 5, CreatedAt: time.Now(), UpdatedAt: time.Now()}, "/api/getApiById", "获取api详细信息", "api", "POST"},
	{global.GVA_MODEL{ID: 6, CreatedAt: time.Now(), UpdatedAt: time.Now()}, "/api/deleteApi", "删除Api", "api", "POST"},
	{global.GVA_MODEL{ID: 7, CreatedAt: time.Now(), UpdatedAt: time.Now()}, "/api/updateApi", "更新Api", "api", "POST"},
	{global.GVA_MODEL{ID: 8, CreatedAt: time.Now(), UpdatedAt: time.Now()}, "/api/getAllApis", "获取所有api", "api", "POST"},
	{global.GVA_MODEL{ID: 9, CreatedAt: time.Now(), UpdatedAt: time.Now()}, "/auth/createAuthority", "创建角色", "auth", "POST"},
	{global.GVA_MODEL{ID: 10, CreatedAt: time.Now(), UpdatedAt: time.Now()}, "/auth/deleteAuthority", "删除角色", "auth", "POST"},
	{global.GVA_MODEL{ID: 11, CreatedAt: time.Now(), UpdatedAt: time.Now()}, "/auth/getAuthorityList", "获取角色列表", "auth", "POST"},
	{global.GVA_MODEL{ID: 12, CreatedAt: time.Now(), UpdatedAt: time.Now()}, "/auth/setDataAuthority", "设置角色资源权限", "auth", "POST"},
	{global.GVA_MODEL{ID: 13, CreatedAt: time.Now(), UpdatedAt: time.Now()}, "/auth/updateAuthority", "更新角色信息", "auth", "PUT"},
	{global.GVA_MODEL{ID: 14, CreatedAt: time.Now(), UpdatedAt: time.Now()}, "/auth/getAuthority", "查询角色信息", "auth", "GET"},
	{global.GVA_MODEL{ID: 15, CreatedAt: time.Now(), UpdatedAt: time.Now()}, "/menu/getMenu", "获取菜单树", "menu", "POST"},
	{global.GVA_MODEL{ID: 16, CreatedAt: time.Now(), UpdatedAt: time.Now()}, "/menu/getMenuList", "分页获取基础menu列表", "menu", "POST"},
	{global.GVA_MODEL{ID: 17, CreatedAt: time.Now(), UpdatedAt: time.Now()}, "/menu/addBaseMenu", "新增菜单", "menu", "POST"},
	{global.GVA_MODEL{ID: 18, CreatedAt: time.Now(), UpdatedAt: time.Now()}, "/menu/getBaseMenuTree", "获取用户动态路由", "menu", "POST"},
	{global.GVA_MODEL{ID: 19, CreatedAt: time.Now(), UpdatedAt: time.Now()}, "/menu/addMenuAuthority", "增加menu和角色关联关系", "menu", "POST"},
	{global.GVA_MODEL{ID: 20, CreatedAt: time.Now(), UpdatedAt: time.Now()}, "/menu/getMenuAuthority", "获取指定角色menu", "menu", "POST"},
	{global.GVA_MODEL{ID: 21, CreatedAt: time.Now(), UpdatedAt: time.Now()}, "/menu/deleteBaseMenu", "删除菜单", "menu", "POST"},
	{global.GVA_MODEL{ID: 22, CreatedAt: time.Now(), UpdatedAt: time.Now()}, "/menu/updateBaseMenu", "更新菜单", "menu", "POST"},
	{global.GVA_MODEL{ID: 23, CreatedAt: time.Now(), UpdatedAt: time.Now()}, "/menu/getBaseMenuById", "根据id获取菜单", "menu", "POST"},
	{global.GVA_MODEL{ID: 24, CreatedAt: time.Now(), UpdatedAt: time.Now()}, "/user/changePassword", "修改密码", "user", "POST"},
	{global.GVA_MODEL{ID: 25, CreatedAt: time.Now(), UpdatedAt: time.Now()}, "/user/getUserList", "获取用户列表", "user", "POST"},
	{global.GVA_MODEL{ID: 26, CreatedAt: time.Now(), UpdatedAt: time.Now()}, "/user/setUserAuthority", "修改用户角色", "user", "POST"},
	{global.GVA_MODEL{ID: 27, CreatedAt: time.Now(), UpdatedAt: time.Now()}, "/user/deleteUser", "删除用户", "user", "DELETE"},
	{global.GVA_MODEL{ID: 28, CreatedAt: time.Now(), UpdatedAt: time.Now()}, "/user/updateUser", "设置用户信息", "user", "PUT"},
	{global.GVA_MODEL{ID: 29, CreatedAt: time.Now(), UpdatedAt: time.Now()}, "/casbin/updateCasbin", "更改角色api权限", "casbin", "POST"},
	{global.GVA_MODEL{ID: 30, CreatedAt: time.Now(), UpdatedAt: time.Now()}, "/casbin/getPolicyPathByAuthorityId", "获取权限列表", "casbin", "POST"},
	{global.GVA_MODEL{ID: 31, CreatedAt: time.Now(), UpdatedAt: time.Now()}, "/jwt/jsonInBlacklist", "jwt加入黑名单", "jwt", "POST"},
	{global.GVA_MODEL{ID: 32, CreatedAt: time.Now(), UpdatedAt: time.Now()}, "/system/getSystemConfig", "获取配置文件内容", "system", "POST"},
	{global.GVA_MODEL{ID: 33, CreatedAt: time.Now(), UpdatedAt: time.Now()}, "/system/setSystemConfig", "设置配置文件内容", "system", "POST"},
	{global.GVA_MODEL{ID: 34, CreatedAt: time.Now(), UpdatedAt: time.Now()}, "/system/getServerInfo", "获取服务器信息", "system", "POST"},
	{global.GVA_MODEL{ID: 35, CreatedAt: time.Now(), UpdatedAt: time.Now()}, "/dictDetail/createDictionaryDetail", "新增字典内容", "dictDetail", "POST"},
	{global.GVA_MODEL{ID: 36, CreatedAt: time.Now(), UpdatedAt: time.Now()}, "/dictDetail/deleteDictionaryDetail", "删除字典内容", "dictDetail", "DELETE"},
	{global.GVA_MODEL{ID: 37, CreatedAt: time.Now(), UpdatedAt: time.Now()}, "/dictDetail/updateDictionaryDetail", "更新字典内容", "dictDetail", "PUT"},
	{global.GVA_MODEL{ID: 38, CreatedAt: time.Now(), UpdatedAt: time.Now()}, "/dictDetail/findDictionaryDetail", "根据ID获取字典内容", "dictDetail", "GET"},
	{global.GVA_MODEL{ID: 39, CreatedAt: time.Now(), UpdatedAt: time.Now()}, "/dictDetail/getDictionaryDetailList", "获取字典内容列表", "dictDetail", "GET"},
	{global.GVA_MODEL{ID: 40, CreatedAt: time.Now(), UpdatedAt: time.Now()}, "/dict/createDictionary", "新增字典", "dict", "POST"},
	{global.GVA_MODEL{ID: 41, CreatedAt: time.Now(), UpdatedAt: time.Now()}, "/dict/deleteDictionary", "删除字典", "dict", "DELETE"},
	{global.GVA_MODEL{ID: 42, CreatedAt: time.Now(), UpdatedAt: time.Now()}, "/dict/updateDictionary", "更新字典", "dict", "PUT"},
	{global.GVA_MODEL{ID: 43, CreatedAt: time.Now(), UpdatedAt: time.Now()}, "/dict/findDictionaryById", "根据ID获取字典", "dict", "GET"},
	{global.GVA_MODEL{ID: 44, CreatedAt: time.Now(), UpdatedAt: time.Now()}, "/dict/getDictionaryList", "获取字典列表", "dict", "GET"},
	{global.GVA_MODEL{ID: 45, CreatedAt: time.Now(), UpdatedAt: time.Now()}, "/sysOperationRecord/createSysOperationRecord", "新增操作记录", "sysOperationRecord", "POST"},
	{global.GVA_MODEL{ID: 46, CreatedAt: time.Now(), UpdatedAt: time.Now()}, "/sysOperationRecord/deleteSysOperationRecord", "删除操作记录", "sysOperationRecord", "DELETE"},
	{global.GVA_MODEL{ID: 47, CreatedAt: time.Now(), UpdatedAt: time.Now()}, "/sysOperationRecord/deleteSysOperationRecordByIds", "批量删除操作记录", "sysOperationRecord", "DELETE"},
	{global.GVA_MODEL{ID: 48, CreatedAt: time.Now(), UpdatedAt: time.Now()}, "/sysOperationRecord/findSysOperationRecord", "根据ID获取操作记录", "sysOperationRecord", "GET"},
	{global.GVA_MODEL{ID: 49, CreatedAt: time.Now(), UpdatedAt: time.Now()}, "/sysOperationRecord/getSysOperationRecordList", "获取操作记录列表", "sysOperationRecord", "GET"},
	{global.GVA_MODEL{ID: 50, CreatedAt: time.Now(), UpdatedAt: time.Now()}, "/email/sendEmail", "发送邮件", "email", "POST"},
	{global.GVA_MODEL{ID: 51, CreatedAt: time.Now(), UpdatedAt: time.Now()}, "/company/createCompany", "添加公司", "company", "POST"},
	{global.GVA_MODEL{ID: 52, CreatedAt: time.Now(), UpdatedAt: time.Now()}, "/company/updateCompany", "更新公司", "company", "PUT"},
	{global.GVA_MODEL{ID: 53, CreatedAt: time.Now(), UpdatedAt: time.Now()}, "/company/deleteCompany", "删除公司", "company", "DELETE"},
	{global.GVA_MODEL{ID: 54, CreatedAt: time.Now(), UpdatedAt: time.Now()}, "/company/getCompanyList", "分页查询公司列表", "company", "GET"},
	{global.GVA_MODEL{ID: 55, CreatedAt: time.Now(), UpdatedAt: time.Now()}, "/company/getCompanyById", "ID查询公司", "company", "GET"},
	{global.GVA_MODEL{ID: 56, CreatedAt: time.Now(), UpdatedAt: time.Now()}, "/company/getCompany", "查询所以公司", "company", "GET"},
	{global.GVA_MODEL{ID: 57, CreatedAt: time.Now(), UpdatedAt: time.Now()}, "/department/createDepartment", "添加部门", "department", "POST"},
	{global.GVA_MODEL{ID: 58, CreatedAt: time.Now(), UpdatedAt: time.Now()}, "/department/updateDepartment", "更新部门", "department", "PUT"},
	{global.GVA_MODEL{ID: 59, CreatedAt: time.Now(), UpdatedAt: time.Now()}, "/department/deleteDepartment", "添加部门", "department", "DELETE"},
	{global.GVA_MODEL{ID: 60, CreatedAt: time.Now(), UpdatedAt: time.Now()}, "/department/getDepartmentList", "分页查询部门列表", "department", "POST"},
	{global.GVA_MODEL{ID: 61, CreatedAt: time.Now(), UpdatedAt: time.Now()}, "/department/getDepartmentById", "ID查询部门", "department", "POST"},
	{global.GVA_MODEL{ID: 62, CreatedAt: time.Now(), UpdatedAt: time.Now()}, "/department/getDepartmentByCode", "公司CODE查询部门", "department", "GET"},
	{global.GVA_MODEL{ID: 63, CreatedAt: time.Now(), UpdatedAt: time.Now()}, "/station/createStation", "添加岗位", "station", "POST"},
	{global.GVA_MODEL{ID: 64, CreatedAt: time.Now(), UpdatedAt: time.Now()}, "/station/updateStation", "更新岗位", "station", "PUT"},
	{global.GVA_MODEL{ID: 65, CreatedAt: time.Now(), UpdatedAt: time.Now()}, "/station/deleteStation", "删除岗位", "station", "DELETE"},
	{global.GVA_MODEL{ID: 66, CreatedAt: time.Now(), UpdatedAt: time.Now()}, "/station/getStationList", "分页查询岗位列表", "station", "GET"},
	{global.GVA_MODEL{ID: 67, CreatedAt: time.Now(), UpdatedAt: time.Now()}, "/station/getStationById", "ID查询岗位", "station", "GET"},
	{global.GVA_MODEL{ID: 68, CreatedAt: time.Now(), UpdatedAt: time.Now()}, "/station/getStationByCode", "公司和部门CODE查询岗位", "station", "GET"},
	{global.GVA_MODEL{ID: 69, CreatedAt: time.Now(), UpdatedAt: time.Now()}, "/cmdb/project/getProjectById", "ID查询项目", "project", "POST"},
	{global.GVA_MODEL{ID: 70, CreatedAt: time.Now(), UpdatedAt: time.Now()}, "/cmdb/project/createProject", "创建项目", "project", "POST"},
	{global.GVA_MODEL{ID: 71, CreatedAt: time.Now(), UpdatedAt: time.Now()}, "/cmdb/project/deleteProject", "删除项目", "project", "DELETE"},
	{global.GVA_MODEL{ID: 72, CreatedAt: time.Now(), UpdatedAt: time.Now()}, "/cmdb/project/getProjectList", "分页查询项目列表", "project", "POST"},
	{global.GVA_MODEL{ID: 73, CreatedAt: time.Now(), UpdatedAt: time.Now()}, "/cmdb/project/updateProject", "更新项目", "project", "PUT"},
	{global.GVA_MODEL{ID: 74, CreatedAt: time.Now(), UpdatedAt: time.Now()}, "/cmdb/project/exportExcel", "导出资产", "project", "GET"},
	{global.GVA_MODEL{ID: 75, CreatedAt: time.Now(), UpdatedAt: time.Now()}, "/loginLogs/deleteLoginLogs", "删除登陆日志", "loginLogs", "DELETE"},
	{global.GVA_MODEL{ID: 76, CreatedAt: time.Now(), UpdatedAt: time.Now()}, "/loginLogs/deleteLoginLogsByIds", "批量删除登陆日志", "loginLogs", "DELETE"},
	{global.GVA_MODEL{ID: 77, CreatedAt: time.Now(), UpdatedAt: time.Now()}, "/loginLogs/getLoginLogsList", "分页查询登陆日志列表", "loginLogs", "GET"},
	{global.GVA_MODEL{ID: 78, CreatedAt: time.Now(), UpdatedAt: time.Now()}, "/cmdb/server/createServer", "创建虚拟主机", "server", "POST"},
	{global.GVA_MODEL{ID: 79, CreatedAt: time.Now(), UpdatedAt: time.Now()}, "/cmdb/server/updateServer", "更新虚拟主机", "server", "PUT"},
	{global.GVA_MODEL{ID: 80, CreatedAt: time.Now(), UpdatedAt: time.Now()}, "/cmdb/server/getServerList", "分页查询虚拟主机列表", "server", "GET"},
	{global.GVA_MODEL{ID: 81, CreatedAt: time.Now(), UpdatedAt: time.Now()}, "/cmdb/server/deleteServer", "删除虚拟主机", "server", "DELETE"},
	{global.GVA_MODEL{ID: 82, CreatedAt: time.Now(), UpdatedAt: time.Now()}, "/cmdb/server/getServerById", "ID查询虚拟主机", "server", "POST"},
	{global.GVA_MODEL{ID: 83, CreatedAt: time.Now(), UpdatedAt: time.Now()}, "/cmdb/server/updateServerByIdAndStatus", "更新虚拟主机状态", "server", "PUT"},
	{global.GVA_MODEL{ID: 84, CreatedAt: time.Now(), UpdatedAt: time.Now()}, "/cmdb/server/getServersByProjectsList", "项目权限查询服务器", "server", "POST"},
	{global.GVA_MODEL{ID: 85, CreatedAt: time.Now(), UpdatedAt: time.Now()}, "/cmdb/server/exportExcel", "导出虚拟主机", "server", "GET"},
	{global.GVA_MODEL{ID: 86, CreatedAt: time.Now(), UpdatedAt: time.Now()}, "/cmdb/security/createSecurity", "增加服务器安全策略", "security", "POST"},
	{global.GVA_MODEL{ID: 87, CreatedAt: time.Now(), UpdatedAt: time.Now()}, "/cmdb/security/updateSecurity", "更新服务器安全策略", "security", "PUT"},
	{global.GVA_MODEL{ID: 88, CreatedAt: time.Now(), UpdatedAt: time.Now()}, "/cmdb/security/deleteSecurity", "删除服务器安全策略", "security", "DELETE"},
	{global.GVA_MODEL{ID: 89, CreatedAt: time.Now(), UpdatedAt: time.Now()}, "/cmdb/security/getSecurityList", "分页查询服务器安全策略列表", "security", "GET"},
	{global.GVA_MODEL{ID: 90, CreatedAt: time.Now(), UpdatedAt: time.Now()}, "/cmdb/database/createDatabase", "创建数据库信息", "database", "POST"},
	{global.GVA_MODEL{ID: 91, CreatedAt: time.Now(), UpdatedAt: time.Now()}, "/cmdb/database/updateDatabase", "更新数据库信息", "database", "PUT"},
	{global.GVA_MODEL{ID: 92, CreatedAt: time.Now(), UpdatedAt: time.Now()}, "/cmdb/database/deleteDatabase", "删除数据库信息", "database", "DELETE"},
	{global.GVA_MODEL{ID: 93, CreatedAt: time.Now(), UpdatedAt: time.Now()}, "/cmdb/database/getDatabaseList", "分页查询数据库信息列表", "database", "GET"},
	{global.GVA_MODEL{ID: 94, CreatedAt: time.Now(), UpdatedAt: time.Now()}, "/cmdb/database/getDatabaseById", "创建数据库信息", "database", "GET"},
	{global.GVA_MODEL{ID: 95, CreatedAt: time.Now(), UpdatedAt: time.Now()}, "/cmdb/database/exportExcel", "导出数据库信息", "database", "GET"},
	{global.GVA_MODEL{ID: 96, CreatedAt: time.Now(), UpdatedAt: time.Now()}, "/cmdb/databaseUser/createDatabaseUser", "创建数据库账户信息", "database", "POST"},
	{global.GVA_MODEL{ID: 97, CreatedAt: time.Now(), UpdatedAt: time.Now()}, "/cmdb/databaseUser/updateDatabaseUser", "更新数据库账户信息", "database", "PUT"},
	{global.GVA_MODEL{ID: 98, CreatedAt: time.Now(), UpdatedAt: time.Now()}, "/cmdb/databaseUser/deleteDatabaseUser", "删除数据库用户信息", "database", "DELETE"},
	{global.GVA_MODEL{ID: 99, CreatedAt: time.Now(), UpdatedAt: time.Now()}, "/cmdb/databaseUser/getDatabaseUserList", "分页查询数据库用户信息列表", "database", "GET"},
	{global.GVA_MODEL{ID: 100, CreatedAt: time.Now(), UpdatedAt: time.Now()}, "/cmdb/databaseUser/getDatabaseUserById", "ID查询数据库用户信息", "database", "GET"},
	{global.GVA_MODEL{ID: 101, CreatedAt: time.Now(), UpdatedAt: time.Now()}, "/cmdb/domain/createDomain", "创建域名信息", "domain", "POST"},
	{global.GVA_MODEL{ID: 102, CreatedAt: time.Now(), UpdatedAt: time.Now()}, "/cmdb/domain/deleteDomain", "删除域名信息", "domain", "DELETE"},
	{global.GVA_MODEL{ID: 103, CreatedAt: time.Now(), UpdatedAt: time.Now()}, "/cmdb/domain/getDomainById", "ID查询域名信息", "domain", "POST"},
	{global.GVA_MODEL{ID: 104, CreatedAt: time.Now(), UpdatedAt: time.Now()}, "/cmdb/domain/updateDomain", "更新域名信息", "domain", "PUT"},
	{global.GVA_MODEL{ID: 105, CreatedAt: time.Now(), UpdatedAt: time.Now()}, "/cmdb/domain/getDomainList", "分页查询域名信息列表", "domain", "POST"},
	{global.GVA_MODEL{ID: 106, CreatedAt: time.Now(), UpdatedAt: time.Now()}, "/cmdb/domain/exportExcel", "导出域名信息", "domain", "GET"},
	{global.GVA_MODEL{ID: 107, CreatedAt: time.Now(), UpdatedAt: time.Now()}, "/cmdb/redis/createCluster", "创建redis集群信息", "redis", "POST"},
	{global.GVA_MODEL{ID: 108, CreatedAt: time.Now(), UpdatedAt: time.Now()}, "/cmdb/redis/deleteCluster", "删除redis集群信息", "redis", "DELETE"},
	{global.GVA_MODEL{ID: 109, CreatedAt: time.Now(), UpdatedAt: time.Now()}, "/cmdb/redis/getClusterList", "分页查询redis集群信息列表", "redis", "POST"},
	{global.GVA_MODEL{ID: 110, CreatedAt: time.Now(), UpdatedAt: time.Now()}, "/cmdb/redis/updateCluster", "更新redis集群信息", "redis", "PUT"},
	{global.GVA_MODEL{ID: 111, CreatedAt: time.Now(), UpdatedAt: time.Now()}, "/cmdb/redis/getClusterById", "ID查询redis集群信息", "redis", "POST"},
	{global.GVA_MODEL{ID: 112, CreatedAt: time.Now(), UpdatedAt: time.Now()}, "/cmdb/redis/exportExcel", "导出redis集群信息", "redis", "GET"},
	{global.GVA_MODEL{ID: 113, CreatedAt: time.Now(), UpdatedAt: time.Now()}, "/cmdb/redis/createRedisDatabase", "创建redis库号信息", "redis", "POST"},
	{global.GVA_MODEL{ID: 114, CreatedAt: time.Now(), UpdatedAt: time.Now()}, "/cmdb/redis/deleteRedisDatabase", "删除redis库号信息", "redis", "DELETE"},
	{global.GVA_MODEL{ID: 115, CreatedAt: time.Now(), UpdatedAt: time.Now()}, "/cmdb/redis/updateRedisDatabase", "更新redis库号信息", "redis", "PUT"},
	{global.GVA_MODEL{ID: 116, CreatedAt: time.Now(), UpdatedAt: time.Now()}, "/cmdb/redis/getRedisDatabaseList", "分页查询redis库号信息列表", "redis", "POST"},
	{global.GVA_MODEL{ID: 117, CreatedAt: time.Now(), UpdatedAt: time.Now()}, "/cmdb/redis/data/getDBList/*", "查询redis集群的库信息", "redis", "GET"},
	{global.GVA_MODEL{ID: 118, CreatedAt: time.Now(), UpdatedAt: time.Now()}, "/cmdb/redis/key/scan", "扫描redis集群中库号的key", "redis", "POST"},
	{global.GVA_MODEL{ID: 119, CreatedAt: time.Now(), UpdatedAt: time.Now()}, "/cmdb/redis/key/query", "查询redis集群中库号的key", "redis", "POST"},
	{global.GVA_MODEL{ID: 120, CreatedAt: time.Now(), UpdatedAt: time.Now()}, "/cmdb/redis/command/sendCommand", "redis终端发送命令", "redis", "POST"},
	{global.GVA_MODEL{ID: 121, CreatedAt: time.Now(), UpdatedAt: time.Now()}, "/basic/getBasicConfigureList", "分页查询基础配置列表", "configure", "GET"},
	{global.GVA_MODEL{ID: 122, CreatedAt: time.Now(), UpdatedAt: time.Now()}, "/basic/createBasicConfigure", "创建基础配置", "configure", "POST"},
	{global.GVA_MODEL{ID: 123, CreatedAt: time.Now(), UpdatedAt: time.Now()}, "/basic/updateBasicConfigure", "更新基础配置", "configure", "PUT"},
	{global.GVA_MODEL{ID: 124, CreatedAt: time.Now(), UpdatedAt: time.Now()}, "/basic/deleteBasicConfigure", "更新基础配置", "configure", "DELETE"},
	{global.GVA_MODEL{ID: 125, CreatedAt: time.Now(), UpdatedAt: time.Now()}, "/deploy/dispose/updateAppConfigure", "更新应用配置", "dispose", "PUT"},
	{global.GVA_MODEL{ID: 126, CreatedAt: time.Now(), UpdatedAt: time.Now()}, "/deploy/dispose/getAppConfigureList", "分页查询应用配置列表", "dispose", "GET"},
	{global.GVA_MODEL{ID: 127, CreatedAt: time.Now(), UpdatedAt: time.Now()}, "/deploy/dispose/createAppConfigure", "创建应用配置", "dispose", "POST"},
	{global.GVA_MODEL{ID: 128, CreatedAt: time.Now(), UpdatedAt: time.Now()}, "/deploy/dispose/deleteAppConfigure", "删除应用配置", "dispose", "DELETE"},
	{global.GVA_MODEL{ID: 129, CreatedAt: time.Now(), UpdatedAt: time.Now()}, "/deploy/dispose/copyAppConfigure", "拷贝应用配置", "dispose", "POST"},
	{global.GVA_MODEL{ID: 130, CreatedAt: time.Now(), UpdatedAt: time.Now()}, "/deploy/app/createDeployApp", "创建应用", "application", "POST"},
	{global.GVA_MODEL{ID: 131, CreatedAt: time.Now(), UpdatedAt: time.Now()}, "/deploy/app/deleteDeployApp", "删除应用", "application", "DELETE"},
	{global.GVA_MODEL{ID: 132, CreatedAt: time.Now(), UpdatedAt: time.Now()}, "/deploy/app/getAppProjectAuthList", "角色ID查询项目", "application", "POST"},
	{global.GVA_MODEL{ID: 133, CreatedAt: time.Now(), UpdatedAt: time.Now()}, "/deploy/app/getDeployAppList", "分页查询应用列表", "application", "GET"},
	{global.GVA_MODEL{ID: 134, CreatedAt: time.Now(), UpdatedAt: time.Now()}, "/deploy/app/updateDeployApp", "更新应用", "application", "PUT"},
	{global.GVA_MODEL{ID: 135, CreatedAt: time.Now(), UpdatedAt: time.Now()}, "/deploy/build/getDeployBuildList", "分页查询构建历史列表", "build", "POST"},
	{global.GVA_MODEL{ID: 136, CreatedAt: time.Now(), UpdatedAt: time.Now()}, "/deploy/build/getAppGitRepoInfo", "查询应用分支tag信息", "build", "GET"},
	{global.GVA_MODEL{ID: 137, CreatedAt: time.Now(), UpdatedAt: time.Now()}, "/deploy/build/getDeployAppBranchList", "查询应用分支信息", "build", "GET"},
	{global.GVA_MODEL{ID: 138, CreatedAt: time.Now(), UpdatedAt: time.Now()}, "/deploy/build/getDeployAppBranchList", "查询应用分支信息", "build", "GET"},
	{global.GVA_MODEL{ID: 139, CreatedAt: time.Now(), UpdatedAt: time.Now()}, "/deploy/build/getAppVirtualList", "查询项目服务器", "build", "GET"},
	{global.GVA_MODEL{ID: 140, CreatedAt: time.Now(), UpdatedAt: time.Now()}, "/deploy/jenkins/createDeployAppJenkins", "创建jenkins任务", "jenkins", "POST"},
	{global.GVA_MODEL{ID: 141, CreatedAt: time.Now(), UpdatedAt: time.Now()}, "/deploy/jenkins/updateDeployAppJenkins", "更新jenkins任务", "jenkins", "PUT"},
	{global.GVA_MODEL{ID: 142, CreatedAt: time.Now(), UpdatedAt: time.Now()}, "/deploy/jenkins/deleteDeployAppJenkins", "删除jenkins任务", "jenkins", "DELETE"},
	{global.GVA_MODEL{ID: 143, CreatedAt: time.Now(), UpdatedAt: time.Now()}, "/deploy/jenkins/startJenkinsJobBuild", "启动jenkins构建任务", "jenkins", "POST"},
	{global.GVA_MODEL{ID: 144, CreatedAt: time.Now(), UpdatedAt: time.Now()}, "/deploy/jenkins/getJenkinsBuildJobLogs", "查询jenkins构建任务日志", "jenkins", "POST"},
	{global.GVA_MODEL{ID: 145, CreatedAt: time.Now(), UpdatedAt: time.Now()}, "/deploy/jenkins/rollbackJenkinsJobBuild", "回滚jenkins任务", "jenkins", "POST"},
	{global.GVA_MODEL{ID: 146, CreatedAt: time.Now(), UpdatedAt: time.Now()}, "/deploy/jenkins/createRollbackJenkinsJob", "创建回滚jenkins任务", "jenkins", "POST"},
	{global.GVA_MODEL{ID: 147, CreatedAt: time.Now(), UpdatedAt: time.Now()}, "/jump/filter/updateJumpServerCmdFilter", "更新过滤命令", "jumpFilter", "PUT"},
	{global.GVA_MODEL{ID: 148, CreatedAt: time.Now(), UpdatedAt: time.Now()}, "/jump/filter/getJumpServerCmdFilterList", "分页查询过滤命令列表", "jumpFilter", "GET"},
	{global.GVA_MODEL{ID: 149, CreatedAt: time.Now(), UpdatedAt: time.Now()}, "/jump/filter/deleteJumpServerCmdFilter", "删除过滤命令", "jumpFilter", "DELETE"},
	{global.GVA_MODEL{ID: 150, CreatedAt: time.Now(), UpdatedAt: time.Now()}, "/jump/filter/createJumpServerCmdFilter", "创建过滤命令", "jumpFilter", "POST"},
	{global.GVA_MODEL{ID: 151, CreatedAt: time.Now(), UpdatedAt: time.Now()}, "/jump/logs/updateJumpServerSshLogs", "更新堡垒机审计日志", "jumpLogs", "PUT"},
	{global.GVA_MODEL{ID: 152, CreatedAt: time.Now(), UpdatedAt: time.Now()}, "/jump/logs/getJumpServerSshLogsList", "分页查询堡垒机审计日志列表", "jumpLogs", "GET"},
	{global.GVA_MODEL{ID: 153, CreatedAt: time.Now(), UpdatedAt: time.Now()}, "/jump/logs/deleteJumpServerSshLogs", "删除堡垒机审计日志", "jumpLogs", "DELETE"},
	{global.GVA_MODEL{ID: 154, CreatedAt: time.Now(), UpdatedAt: time.Now()}, "/jump/logs/deleteJumpServerSshLogsByIds", "批量删除堡垒机审计日志", "jumpLogs", "DELETE"},
	{global.GVA_MODEL{ID: 155, CreatedAt: time.Now(), UpdatedAt: time.Now()}, "/jump/user/getJumpServerUserById", "ID查询堡垒机用户的服务器", "jumpUser", "GET"},
	{global.GVA_MODEL{ID: 156, CreatedAt: time.Now(), UpdatedAt: time.Now()}, "/jump/user/updateJumpServerUser", "更新堡垒机用户的服务器别名", "jumpUser", "PUT"},
	{global.GVA_MODEL{ID: 157, CreatedAt: time.Now(), UpdatedAt: time.Now()}, "/jump/user/deleteJumpServerUserByIds", "批量删除堡垒机用户的服务器", "jumpUser", "DELETE"},
	{global.GVA_MODEL{ID: 158, CreatedAt: time.Now(), UpdatedAt: time.Now()}, "/jump/user/createJumpServerUser", "创建堡垒机用户的服务器", "jumpUser", "POST"},
	{global.GVA_MODEL{ID: 159, CreatedAt: time.Now(), UpdatedAt: time.Now()}, "/jump/user/deleteJumpServerUser", "删除堡垒机用户的服务器", "jumpUser", "DELETE"},
	{global.GVA_MODEL{ID: 160, CreatedAt: time.Now(), UpdatedAt: time.Now()}, "/jump/user/getJumpServerUserList", "分页查询堡垒机用户的服务器列表", "jumpUser", "GET"},
	{global.GVA_MODEL{ID: 161, CreatedAt: time.Now(), UpdatedAt: time.Now()}, "/casbin/updateProjectCasbin", "更新角色的项目权限", "casbin", "POST"},
	{global.GVA_MODEL{ID: 162, CreatedAt: time.Now(), UpdatedAt: time.Now()}, "/casbin/getPolicyProjectByAuthorityId", "角色Id查询项目", "casbin", "GET"},

}

func InitSysApi(db *gorm.DB) {
	if err := db.Transaction(func(tx *gorm.DB) error {
		if tx.Where("id IN ?", []int{1, 162}).Find(&[]model.SysApi{}).RowsAffected == 2 {
			color.Danger.Println("sys_apis表的初始数据已存在!")
			return nil
		}
		if err := tx.Create(&Apis).Error; err != nil { // 遇到错误时回滚事务
			return err
		}
		return nil
	}); err != nil {
		color.Warn.Printf("[Mysql]--> sys_apis 表的初始数据失败,err: %v\n", err)
		os.Exit(0)
	}
}
