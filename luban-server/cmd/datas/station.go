package datas

import (
	"gin-luban-server/global"
	"gin-luban-server/model"
	"github.com/gookit/color"
	"gorm.io/gorm"
	"os"
	"time"
)

func InitSysStation(db *gorm.DB) {
	status := new(bool)
	*status = true
	Stations := []model.SysStation{
		{GVA_MODEL: global.GVA_MODEL{ID: 1, CreatedAt: time.Now(), UpdatedAt: time.Now()},StationCode: "100001",StationName: "应用运维",Status: status,CompanyCode: "100001",DeptCode: "100001",Remark: "应用运维"},
	}
	if err := db.Transaction(func(tx *gorm.DB) error {
		if tx.Where("id IN ?", []int{1}).Find(&[]model.SysUser{}).RowsAffected == 2 {
			color.Danger.Println("sys_station表的初始数据已存在!")
			return nil
		}
		if err := tx.Create(&Stations).Error; err != nil { // 遇到错误时回滚事务
			return err
		}
		return nil
	}); err != nil {
		color.Warn.Printf("[Mysql]--> sys_station 表的初始数据失败,err: %v\n", err)
		os.Exit(0)
	}
}
