package service

import (
	"errors"
	"gin-luban-server/global"
	"gin-luban-server/model"
	"gin-luban-server/model/request"
)

//@author: heyibo
//@function: CreateJumpServerUser
//@description: 创建堡垒机服务器
//@param: data JumpServerUser
//@return: err error, data model.JumpServerUser

func CreateJumpServerUser(data request.JumpServerUserFrom)(err error)  {
	var basic model.SysBasicConfigure
	err = global.GVA_DB.Model(&model.SysBasicConfigure{}).Where("purpose = ? ",data.OsUser).First(&basic).Error
	if err != nil {
		return errors.New("查询基础配置表失败！")
	}
	if len(data.VirtualCodeInfos) >0 {
		for _, v := range data.VirtualCodeInfos {
			var serverUser model.JumpServerUser
			serverUser.UserName = data.UserName
			serverUser.SshUser = basic.BasicUser
			serverUser.SshPassword = basic.BasicPasswd
			serverUser.SshType = basic.SshType
			serverUser.SshKey = basic.SshKey
			var virtual model.CmdbServer
            err = global.GVA_DB.Where("virtual_code = ?",v.VirtualCode).First(&virtual).Error
            if err != nil {
				return errors.New("查询cmdb服务器表失败！")
			}
			serverUser.VirtualCode = virtual.VirtualCode
			serverUser.SshIpAddress = virtual.Ipaddress
			serverUser.ProjectCode = virtual.ProjectCode
			serverUser.SshPort = virtual.Port
			serverUser.XShellProxy =virtual.XShellProxy
			if virtual.XShellProxy != "connect" {
				var proxy model.SysBasicConfigure
				err = global.GVA_DB.Model(&model.SysBasicConfigure{}).Where("purpose = ? ",virtual.XShellProxy).First(&proxy).Error
				if err != nil {
					return errors.New("查询代理服务器失败！")
				}
				serverUser.ProxySshType = proxy.SshType
				serverUser.ProxyUser = proxy.BasicUser
				serverUser.ProxyHost = proxy.ProxyHost
				serverUser.ProxyPort = proxy.ProxyPort
				serverUser.ProxyPassword = proxy.BasicPasswd
				serverUser.ProxySshKey = proxy.SshKey
			}
			var total int64
			err = global.GVA_DB.Model(&model.JumpServerUser{}).Where("virtual_code = ? AND user_name = ?",v.VirtualCode,data.UserName).Count(&total).Error
			if total > 0 {
               err = global.GVA_DB.Model(&model.JumpServerUser{}).Where("virtual_code = ? AND user_name = ?",v.VirtualCode,data.UserName).Updates(&serverUser).Error
			}else {
				err = global.GVA_DB.Create(&serverUser).Error
			}
			if err != nil {
				break
			}
		}
	}else {
		return errors.New("请选择服务器！")
	}
	return err
}

//@author: heyibo
//@function: DeleteJumpServerUser
//@description: 删除堡垒机服务器
//@param: id ,jumpUser model.JumpServerUser
//@return: err error
func DeleteJumpServerUser(id float64) (err error) {
	var jumpUser model.JumpServerUser
	db := global.GVA_DB.Where("id = ?", id).First(&jumpUser)
	err = db.Delete(&jumpUser).Error
	return err
}


//@author: heyibo
//@function: DeleteJumpServerUserByIds
//@description: 批量删除记录
//@param: ids request.IdsReq
//@return: err error

func DeleteJumpServerUserByIds(ids request.IdsReq) (err error) {
	err = global.GVA_DB.Delete(&[]model.JumpServerUser{}, "id in (?)", ids.Ids).Error
	return err
}

//@author: heyibo
//@function: GetJumpServerUserInfoList
//@description: 分页获取数据
//@param: info request.PageInfo
//@return: err error, list interface{}, total int64

func GetJumpServerUserInfoList(info request.SearchJumpServerUserParams) (err error, list interface{}, total int64) {
	limit := info.PageSize
	offset := info.PageSize * (info.Page - 1)
	db := global.GVA_DB.Model(&model.JumpServerUser{})
	var List []model.JumpServerUser
	if info.SshIpAddress != "" {
		db = db.Where("ssh_ip_address = ?", info.SshIpAddress)
	}
	if info.UserName != "" {
		db = db.Where("user_name = ?", info.UserName)
	}
	err = db.Count(&total).Error

	if err != nil {
		return err, List, total
	} else {
		err = db.Limit(limit).Offset(offset).Preload("CMDBServers").Preload("Project").Find(&List).Error
	}
	return err, List, total
}

//@author: heyibo
//@function: GetJumpServerUserInfoList
//@description: 分页获取数据
//@param: info request.PageInfo
//@return: err error, list interface{}, total int64

func GetJumpServerUserById(Id float64) (err error, List model.JumpServerUser) {
	db := global.GVA_DB.Model(&model.JumpServerUser{})
	db = db.Where("id = ?", Id)
    err = db.Preload("CMDBServers").First(&List).Error
	return err, List
}
//@author: heyibo
//@function: UpdateJumpServerUser
//@description: 添加别名
//@param: username string,id float64,alias string
//@return: err error

func UpdateJumpServerUser(userName string,id float64,alias string ) (err error)  {
	var servers model.JumpServerUser
	err = global.GVA_DB.Where("user_name = ? AND id = ?",userName,id).First(&servers).Error
	if err != nil {
		return errors.New("没查询到信息")
	}
    servers.VirtualAlias = alias
	err = global.GVA_DB.Where("id = ?", id).First(&model.JumpServerUser{}).Updates(&servers).Error
	return err
}