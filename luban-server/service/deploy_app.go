package service

import (
	"errors"
	"gin-luban-server/global"
	"gin-luban-server/model"
	"gin-luban-server/model/request"
	"gorm.io/gorm"
)

//@author: heyibo
//@function: CreateDeployApp
//@description: 创建ci/cd树信息
//@param: apply DeployApp
//@return: err error, apply model.DeployApp

func CreateDeployApp(apply model.DeployApp) (err error) {
	if !errors.Is(global.GVA_DB.Where("apps_name =  ?", apply.AppsName).First(&model.DeployApp{}).Error, gorm.ErrRecordNotFound) {
		return errors.New("存在相同应用名称")
	}
	return global.GVA_DB.Create(&apply).Error
}

//@author: heyibo
//@function: DeleteDeployApp
//@description: 删除ci/cd树信息
//@param: id ,apply model.DeployApp
//@return: err error

func DeleteDeployApp(id float64) (err error) {
	var apply model.DeployApp
	db := global.GVA_DB.Where("id = ?", id).First(&apply)
	err = db.Delete(&apply).Error
	return err
}

//@author: heyibo
//@function: UpdateDeployApp
//@description: 更改一个角色
//@param: DeployApp model.DeployApp
//@return:err error, DeployApp model.DeployApp

func UpdateDeployApp(DeployApp model.DeployApp) (err error) {
	var total int64
	var apps []model.DeployApp
	err = global.GVA_DB.Where("apps_name = ? AND id != ?",DeployApp.AppsName,DeployApp.ID).Find(&apps).Count(&total).Error
	if total >= 1 {
		return errors.New("存在相同应用名称")
	}
	// 通过唯一ID进行更新
	err = global.GVA_DB.Where("id = ?", DeployApp.ID).First(&model.DeployApp{}).Updates(&DeployApp).Error
	return err
}

//@author: heyibo
//@function: GetDeployAppInfoList
//@description: 分页获取数据
//@param: info request.PageInfo
//@return: err error, list interface{}, total int64

func GetDeployAppInfoList(info request.SearchDeployAppParams) (err error, list interface{}, total int64) {
	limit := info.PageSize
	offset := info.PageSize * (info.Page - 1)
	db := global.GVA_DB.Model(&model.DeployApp{})
	var applysList []model.DeployApp

	if info.AppsName != "" {
		db = db.Where("apps_name = ?", info.AppsName)
	}
	if info.ProjectCode != "" {
		db = db.Where("project_code = ?", info.ProjectCode)
	}
	err = db.Count(&total).Error

	if err != nil {
		return err, applysList, total
	} else {
		db = db.Limit(limit).Offset(offset)
		err = db.Preload("Project").Find(&applysList).Error
	}
	return err, applysList, total
}

//@author: heyibo
//@function: UpdateDeployAppVirtual
//@description: 更新应用、环境、服务器关联表
//@return: err error, applyVirtual model.DeployAppVirtual
//func UpdateDeployAppVirtual(applyVirtual request.DeployAppVirtualFrom) (err error) {
//	var ClearApplyVirtual []model.DeployAppVirtual
//	err = global.GVA_DB.Model(&model.DeployAppVirtual{}).Where("apps_code = ? And deploy_env = ? ",applyVirtual.AppsCode,applyVirtual.DeployEnv).Find(&ClearApplyVirtual).Error
//	if len(ClearApplyVirtual) > 0 {
//		if err =global.GVA_DB.Model(&model.DeployAppVirtual{}).Where("apps_code = ? And deploy_env = ?",applyVirtual.AppsCode,applyVirtual.DeployEnv).Delete(ClearApplyVirtual).Error;err !=nil{
//			return errors.New("删除项目服务器配置错误！")
//		}
//	}
//	for _, v := range applyVirtual.VirtualCodeInfos {
//		virtual := model.DeployAppVirtual{
//			AppsCode: applyVirtual.AppsCode,
//			DeployEnv: applyVirtual.DeployEnv,
//			VirtualCode: v.VirtualCode,
//		}
//		err = global.GVA_DB.Create(&virtual).Error
//		if err != nil {
//			break
//		}
//	}
//	return err
//}

//@author: heyibo
//@function: UpdateDeployAppVirtual
//@description: 更新应用、环境、服务器关联表
//@return: err error, applyVirtual model.DeployAppVirtual

//func GetDeployAppVirtualList(info request.SearchDeployAppVirtualParams) (err error, list interface{}, total int64) {
//	limit := info.PageSize
//	offset := info.PageSize * (info.Page - 1)
//	db := global.GVA_DB.Model(&model.DeployAppVirtual{})
//	var applysList []model.DeployAppVirtual
//	err = db.Count(&total).Error
//	if info.AppsCode != "" {
//		db = db.Where("apps_code = ?", info.AppsCode)
//	}
//	if info.DeployEnv != "" {
//		db = db.Where("deploy_env = ?",info.DeployEnv)
//	}
//	if err != nil {
//		return err, applysList, total
//	} else {
//		db = db.Limit(limit).Offset(offset)
//		err = db.Find(&applysList).Error
//	}
//	return err, applysList, total
//}

