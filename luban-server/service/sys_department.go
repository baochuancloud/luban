package service

import (
	"errors"
	"gin-luban-server/global"
	"gin-luban-server/model"
	"gin-luban-server/model/request"
	"gorm.io/gorm"
)

//@author: heyibo
//@function: CreateDepartment
//@description: 创建一个部门
//@param: auth model.SysDepartment
//@return: err error, department model.SysDepartment

func CreateDepartment(department model.SysDepartment) (err error) {
	if !errors.Is(global.GVA_DB.Where("dept_code = ?", department.DeptCode).First(&department).Error, gorm.ErrRecordNotFound) {
		return errors.New("存在相同部门编码")
	}
	err = global.GVA_DB.Create(&department).Error
	return err
}

//@author: heyibo
//@function: UpdateDepartment
//@description: 更新部门信息
//@param: auth model.SysDepartment
//@return:err error, station model.SysDepartment

func UpdateDepartment(depart model.SysDepartment) (err error) {
	var total int64
	var departList []model.SysDepartment
	err = global.GVA_DB.Where("dept_code = ? AND id != ?",depart.DeptCode,depart.ID).Find(&departList).Count(&total).Error
	if total >= 1 {
		return errors.New("存在相同部门编码")
	}
	// 通过唯一ID进行更新
	err = global.GVA_DB.Where("id = ?", depart.ID).First(&model.SysDepartment{}).Updates(&depart).Error
	return err
}


//@author: heyibo
//@function: DeleteDepartment
//@description: 删除部门信息
//@param: auth *model.SysDepartment
//@return: err error

func DeleteDepartment(dept * model.SysDepartment) (err error) {
	db := global.GVA_DB.Preload("SysUsers").Preload("SysStations").First(dept)
	if len(dept.SysUsers) > 0 {
		return errors.New("有用户正在使用禁止删除")
	}else if len(dept.SysStations) > 0 {
		return errors.New("有岗位正在使用禁止删除")
	}
	err = db.Delete(&dept).Error
	return err
}

//@author: heyibo
//@function: GetDepartmentInfoList
//@description: 分页获取数据
//@param: info request.PageInfo
//@return: err error, list interface{}, total int64

func GetDepartmentInfoList(info request.SysDepartmentSearch) (err error, list interface{}, total int64) {
	limit := info.PageSize
	offset := info.PageSize * (info.Page - 1)
	db := global.GVA_DB.Model(&model.SysDepartment{})
	var department []model.SysDepartment

	if info.DeptName != "" {
		db = db.Where("dept_name LIKE ?", "%"+info.DeptName+"%")
	}
	if info.DeptCode != "" {
		db = db.Where("dept_code = ?", info.DeptCode)
	}
	if info.Status != nil {
		db = db.Where("status = ?", info.Status)
	}
	err = db.Count(&total).Error
	err = db.Limit(limit).Offset(offset).Preload("Company").Find(&department).Error
	return err, department, total
}


//@author: heyibo
//@function: FindDepartmentById
//@description: 通过id获取公司信息
//@param: id int
//@return: err error, user *model.SysDepartment

func GetDepartmentById(id float64) (err error, depart model.SysDepartment) {
	err = global.GVA_DB.Where("id = ?", id).First(&depart).Error
	return err, depart
}
//@author: heyibo
//@function: FindDepartmentById
//@description: 通过id获取公司信息
//@param: id int
//@return: err error, user *model.SysDepartment

func FindDepartmentByCode(code string) (err error, depart []model.SysDepartment) {
	err = global.GVA_DB.Model(&model.SysDepartment{}).Where("`company_code` =  ? ",code).Find(&depart).Error
	return err, depart
}
