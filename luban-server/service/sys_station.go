package service

import (
	"errors"
	"gin-luban-server/global"
	"gin-luban-server/model"
	"gin-luban-server/model/request"
	"gorm.io/gorm"
)

//@author: heyibo
//@function: CreateStation
//@description: 创建一个岗位
//@param: auth model.SysStation
//@return: err error, station model.SysStation

func CreateStation(station model.SysStation) (err error) {
	if !errors.Is(global.GVA_DB.Where("station_code = ?", station.StationCode).First(&model.SysStation{}).Error, gorm.ErrRecordNotFound) {
		return errors.New("存在相同岗位编码")
	}
	err = global.GVA_DB.Create(&station).Error
	return err
}

//@author: heyibo
//@function: UpdateStation
//@description: 更新岗位信息
//@param: auth model.SysStation
//@return:err error, station model.Station

func UpdateStation(station model.SysStation) (err error) {
	var total int64
	var stationList []model.SysStation
	err = global.GVA_DB.Where("station_code = ? AND id != ?",station.StationCode,station.ID).Find(&stationList).Count(&total).Error
	if total >= 1 {
		return errors.New("存在相同岗位编码")
	}
	err = global.GVA_DB.Where("id = ?", station.ID).First(&model.SysStation{}).Updates(&station).Error
	return err
}

//@author: heyibo
//@function: DeleteStation
//@description: 删除岗位信息
//@param: auth *model.Station
//@return: err error

func DeleteStation(station * model.SysStation) (err error) {
	db := global.GVA_DB.Preload("SysUsers").First(station)
	if len(station.SysUsers) > 0 {
		return errors.New("有用户正在使用禁止删除")
	}
	err = db.Delete(&station).Error
	return err
}

//@author: heyibo
//@function: GetStationInfoList
//@description: 分页获取数据
//@param: info request.PageInfo
//@return: err error, list interface{}, total int64

func GetStationInfoList(info request.SysStationSearch) (err error, list interface{}, total int64) {
	limit := info.PageSize
	offset := info.PageSize * (info.Page - 1)
	db := global.GVA_DB.Model(&model.SysStation{})
	var station []model.SysStation

	if info.StationName != "" {
		db = db.Where("`station_name` LIKE ?", "%"+info.StationName+"%")
	}
	if info.StationCode != "" {
		db = db.Where("`station_code` LIKE ?", "%"+info.StationCode+"%")
	}
	if info.Status != nil {
		db = db.Where("status = ?", info.Status)
	}
	err = db.Count(&total).Error
	err = db.Limit(limit).Offset(offset).Preload("Company").Preload("Department").Find(&station).Error
	return err, station, total
}

//@author: heyibo
//@function: GetStationById
//@description: 通过Id查询数据
//@param:  request.GetById
//@return: err error, err error, station *model.SysStation

func FindStationById(id float64) (err error, station *model.SysStation) {
	err = global.GVA_DB.Where("`id` =  ? ", id).First(&station).Error
	return err, station
}

//@author: heyibo
//@function: GetStationByCode
//@description: 通过公司和部门code查询数据
//@param:  request.GetCompanyAndDeptCode
//@return: err error, err error, station []model.SysStation

func FindStationByCode(code request.GetCompanyAndDeptCode) (err error, station []model.SysStation) {
	err = global.GVA_DB.Model(&model.SysStation{}).Where("`company_code` =  ? and `dept_code` = ? ",code.CompanyCode,code.DeptCode).Find(&station).Error
	return err, station
}
