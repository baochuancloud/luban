export const sortEnv = (a, b) => {
  switch (a.deploy_env) {
    case 'dev':
      a.priority = 1
      break
    case 'test':
      a.priority = 2
      break
    case 'uat':
      a.priority = 3
      break
    case 'prod':
      a.priority = 4
      break
    default:
      a.priority = 5
  }
  switch (b.deploy_env) {
    case 'dev':
      b.priority = 1
      break
    case 'test':
      b.priority = 2
      break
    case 'uat':
      b.priority = 3
      break
    case 'prod':
      b.priority = 4
      break
    default:
      b.priority = 5
  }
  return a.priority - b.priority

}