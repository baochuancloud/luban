import service from '@/utils/request'
// @Tags server
// @Summary 分页安全策略
// @Security ApiKeyAuth
// @accept application/json
// @Produce application/json
// @Param data body modelInterface.PageInfo true "分页安全策略"
// @Success 200 {string} json "{"success":true,"data":{},"msg":"获取成功"}"
// @Router/cmdb/security/getsecuritylist [get]
// {
//  page     int
//	pageSize int
// }
export const getSecurityList = (params) => {
  return service({
    url: "/cmdb/security/getSecurityList",
    method: 'get',
    params
  })
}


// @Tags server
// @Summary 分页安全策略
// @Security ApiKeyAuth
// @accept application/json
// @Produce application/json
// @Param data body modelInterface.PageInfo true "分页安全策略"
// @Success 200 {string} json "{"success":true,"data":{},"msg":"获取成功"}"
// @Router/cmdb/security/createSecurity [get]
export const createSecurityDetail = (data) => {
  return service({
    url: "/cmdb/security/createSecurity",
    method: 'post',
    data
  })
}


// @Tags server
// @Summary 分页安全策略
// @Security ApiKeyAuth
// @accept application/json
// @Produce application/json
// @Param data body modelInterface.PageInfo true "修改安全策略"
// @Success 200 {string} json "{"success":true,"data":{},"msg":"获取成功"}"
// @Router/cmdb/security/updateSecurity [update]
export const updateSecurityDetail = (data) => {
  return service({
    url: "/cmdb/security/updateSecurity",
    method: 'put',
    data
  })
}


// @Tags server
// @Summary 分页安全策略
// @Security ApiKeyAuth
// @accept application/json
// @Produce application/json
// @Param data body modelInterface.PageInfo true "删除安全策略"
// @Success 200 {string} json "{"success":true,"data":{},"msg":"获取成功"}"
// @Router/cmdb/security/deleteSecurity [delete]

export const deleteSecurityDetail = (data) => {
  return service({
    url: "/cmdb/security/deleteSecurity",
    method: 'delete',
    data
  })
}


// @Tags Api
// @Summary 导出虚拟机信息
// @Security ApiKeyAuth
// @accept application/json
// @Produce application/json
// @Param data body api.exportExcel true ""
// @Success 200 {string} json "{"success":true,"data":{},"msg":"添加成功"}"
// @Router /cmdb/server/exportExcel [get]

export const exportExcel = () => {
  return service({
      url: "/cmdb/server/exportExcel",
      method: 'get',
  })
}

