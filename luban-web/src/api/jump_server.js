import service from '@/utils/request'
// import { handleFileError } from '@/utils/download'
// @Tags server
// @Summary 获取服务器树
// @Security ApiKeyAuth
// @accept application/json
// @Produce application/json
// @Param data body modelInterface.PageInfo true "分页获取server列表"
// @Success 200 {string} json "{"success":true,"data":{},"msg":"获取成功"}"
// @Router /jump/server/getJumpServerTree [get]

export const getJumpServerTree = (params) => {
  return service({
    url: "/jump/server/getJumpServerTree",
    method: 'get',
    params
  })
}


// export const  ShellWs= (params) => {
//     return service({
//         url: "/jump/server/ws",
//         method: 'get',
//         params
//     })
// }
