import service from '@/utils/request'

// @Tags NginxDomainCert
// @Summary 创建NginxDomainCert
// @Security ApiKeyAuth
// @accept application/json
// @Produce application/json
// @Param data body model.NginxDomainCert true "创建NginxDomainCert"
// @Success 200 {string} string "{"success":true,"data":{},"msg":"获取成功"}"
// @Router /nginx/Cert/createNginxDomainCert [post]
export const createNginxDomainCert = (data) => {
  return service({
    url: "/nginx/Cert/createNginxDomainCert",
    method: 'post',
    data
  })
}


// @Tags NginxDomainCert
// @Summary 删除NginxDomainCert
// @Security ApiKeyAuth
// @accept application/json
// @Produce application/json
// @Param data body model.NginxDomainCert true "删除NginxDomainCert"
// @Success 200 {string} string "{"success":true,"data":{},"msg":"删除成功"}"
// @Router /nginx/Cert/deleteNginxDomainCert [delete]
export const deleteNginxDomainCert = (data) => {
  return service({
    url: "/nginx/Cert/deleteNginxDomainCert",
    method: 'delete',
    data
  })
}


// 更新接口暂时不需要
export  const updateNginxDomainCert = (data) => {
  return null

}
// @Tags NginxDomainCert
// @Summary 删除NginxDomainCert
// @Security ApiKeyAuth
// @accept application/json
// @Produce application/json
// @Param data body request.IdsReq true "批量删除NginxDomainCert"
// @Success 200 {string} string "{"success":true,"data":{},"msg":"删除成功"}"
// @Router /nginx/Cert/deleteNginxDomainCert [delete]
export const deleteNginxDomainCertByIds = (data) => {
  return service({
    url: "/nginx/Cert/deleteNginxDomainCertByIds",
    method: 'delete',
    data
  })
}



// @Tags NginxDomainCert
// @Summary 用id查询NginxDomainCert
// @Security ApiKeyAuth
// @accept application/json
// @Produce application/json
// @Param data body model.NginxDomainCert true "用id查询NginxDomainCert"
// @Success 200 {string} string "{"success":true,"data":{},"msg":"查询成功"}"
// @Router /Cert/findNginxDomainCert [get]
export const findNginxDomainCert = (params) => {
  return service({
    url: "/nginx/Cert/getNginxDomainCertById",
    method: 'post',
    params
  })
}


// @Tags NginxDomainCert
// @Summary 分页获取NginxDomainCert列表
// @Security ApiKeyAuth
// @accept application/json
// @Produce application/json
// @Param data body request.PageInfo true "分页获取NginxDomainCert列表"
// @Success 200 {string} string "{"success":true,"data":{},"msg":"获取成功"}"
// @Router /nginx/Cert/getNginxDomainCertList [get]
export const getNginxDomainCertList = (params) => {
  return service({
    url: "/nginx/Cert/getNginxDomainCertList",
    method: 'post',
    params
  })
}