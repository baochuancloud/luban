import service from '@/utils/request'
import { handleFileError } from '@/utils/download'

// @Tags CMDBDomain
// @Summary 新增项目
// @Router /cmdb/domain/createDomain[post]
export const createDomain = (data) => {
  return service({
    url: "/cmdb/domain/createDomain",
    method: 'post',
    data
  })
}

// @Tags CMDBDomain
// @Summary 删除项目
// @Router /cmdb/domain/deleteDomain [delete]
export const deleteDomain = (data) => {
  return service({
    url: "/cmdb/domain/deleteDomain",
    method: 'delete',
    data: data
  })
}



// @Tags CMDBDomain
// @Summary 更新项目
// @Router /cmdb/domain/updateDomain[put]
export const updateDomain = (data) => {
  return service({
    url: "/cmdb/domain/updateDomain",
    method: 'put',
    data: data
  })
}


// @Tags CMDBDomain
// @Summary 获取项目列表
// @Router /cmdb/domain/getDomainList[get]
export const getDomainList = (data) => {
  return service({
    url: "/cmdb/domain/getDomainList",
    method: 'post',
    data
  })
}

// @Tags CMDBDomain
// @Summary 通过ID获取项目详情
// @Router /cmdb/domain/getDomainById[post]
export const getDomainById = (data) => {
  return service({
    url: "/cmdb/domain/getDomainById",
    method: 'post',
    data
  })
}

// @Tags CMDBDomain
// @Summary Export
// @Router /cmdb/domain/exportExcel[get]
export const exportExcel = (params) => {
  return service({
    url: "/cmdb/domain/exportExcel",
    method: 'get',
    params,
    responseType: 'blob'   // blob文件对象
  }).then(res => {
    handleFileError(res, params.fileName)
  })
}

// @Tags CMDBDomain
// @Summary 通过ID获取项目详情
// @Router cmdb/domain/getDomain [get]
export const getDomain = (params) => {
  return service({
    url: "/cmdb/domain/getDomain",
    method: 'get',
    params
  })
}
