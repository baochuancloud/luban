import service from '@/utils/request'

// @Tags SysLoginLogs
// @Summary 删除SysLoginLogs
// @Security ApiKeyAuth
// @accept application/json
// @Produce application/json
// @Param data body model.SysLoginLogs true "删除SysLoginLogs"
// @Success 200 {string} string "{"success":true,"data":{},"msg":"删除成功"}"
// @Router /loginLogs/deleteLoginLogs [delete]
export const deleteLoginLogs = (data) => {
    return service({
        url: "/loginLogs/deleteLoginLogs",
        method: 'delete',
        data
    })
}

// @Tags SysLoginLogs
// @Summary 批量删除SysLoginLogs
// @Security ApiKeyAuth
// @accept application/json
// @Produce application/json
// @Param data body request.IdsReq true "删除SysLoginLogs"
// @Success 200 {string} string "{"success":true,"data":{},"msg":"删除成功"}"
// @Router /loginLogs/deleteLoginLogsByIds [delete]
export const deleteLoginLogsByIds = (data) => {
    return service({
        url: "/loginLogs/deleteLoginLogsByIds",
        method: 'delete',
        data
    })
}

// @Tags SysLoginLogs
// @Summary 分页获取SysLoginLogs列表
// @Security ApiKeyAuth
// @accept application/json
// @Produce application/json
// @Param data body request.PageInfo true "分页获取SysLoginLogs列表"
// @Success 200 {string} string "{"success":true,"data":{},"msg":"获取成功"}"
// @Router /loginLogs/getLoginLogsList [get]
export const getLoginLogsList = (params) => {
    return service({
        url: "/loginLogs/getLoginLogsList",
        method: 'get',
        params
    })
}
