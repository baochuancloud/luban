import service from '@/utils/request'
import { handleFileError } from '@/utils/download'

// @Tags CMDBProject
// @Summary 新增项目
// @Router /cmdb/project/createProject[post]
export const createProject = (data) => {
  return service({
    url: "/cmdb/project/createProject",
    method: 'post',
    data
  })
}

// @Tags CMDBProject
// @Summary 删除项目
// @Router /cmdb/project/deleteProject [delete]
export const deleteProject = (data) => {
  return service({
    url: "/cmdb/project/deleteProject",
    method: 'delete',
    data: data
  })
}



// @Tags CMDBProject
// @Summary 更新项目
// @Router /cmdb/project/updateProject[put]
export const updateProject = (data) => {
  return service({
    url: "/cmdb/project/updateProject",
    method: 'put',
    data: data
  })
}


// @Tags CMDBProject
// @Summary 获取项目列表
// @Router /cmdb/project/getProjectList[get]
export const getProjectList = (data) => {
  return service({
    url: "/cmdb/project/getProjectList",
    method: 'post',
    data
  })
}

// @Tags CMDBProject
// @Summary 通过ID获取项目详情
// @Router /cmdb/project/getProjectById[post]
export const getProjectById = (data) => {
  return service({
    url: "/cmdb/project/getProjectById",
    method: 'post',
    data
  })
}

// @Tags CMDBProject
// @Summary Export
// @Router /cmdb/project/exportExcel[get]
export const exportExcel = (params) => {
  return service({
    url: "/cmdb/project/exportExcel",
    method: 'get',
    params,
    responseType: 'blob'   // blob文件对象
  }).then(res => {
    handleFileError(res, params.fileName)
  })
}


