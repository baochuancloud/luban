import service from '@/utils/request'
// @Tags api
// @Summary 分页获取过滤规则列表
// @Security ApiKeyAuth
// @accept application/json
// @Produce application/json
// @Param data body model.JumpServerCmdFilter.PageInfo true "分页获取用户列表"
// @Success 200 {string} json "{"success":true,"data":{},"msg":"获取成功"}"
// @Router /jump/filter/getJumpServerCmdFilterList [Get]
// {
//  page     int
//	pageSize int
// }
export const getJumpServerCmdFilterList = (params) => {
    return service({
        url: "/jump/filter/getJumpServerCmdFilterList",
        method: 'get',
        params
    })
}


// @Tags Api
// @Summary 更新过滤规则
// @Security ApiKeyAuth
// @accept application/json
// @Produce application/json
// @Param data body api.updateJumpServerCmdFilter true "更新api"
// @Success 200 {string} json "{"success":true,"data":{},"msg":"更新成功"}"
// @Router /jump/filter/updateJumpServerCmdFilter [post]
export const updateJumpServerCmdFilter = (data) => {
    return service({
        url: "/jump/filter/updateJumpServerCmdFilter",
        method: 'put',
        data
    })
}


// @Tags Api
// @Summary 新增过滤规则
// @Security ApiKeyAuth
// @accept application/json
// @Produce application/json
// @Param data body api.JumpServerUserParams true "新增公司"
// @Success 200 {string} json "{"success":true,"data":{},"msg":"添加成功"}"
// @Router /jump/filter/createJumpServerCmdFilter [post]

export const createJumpServerCmdFilter = (data) => {
    return service({
        url: "/jump/filter/createJumpServerCmdFilter",
        method: 'post',
        data
    })
}



// @Tags Api
// @Summary 删除过滤规则
// @Security ApiKeyAuth
// @accept application/json
// @Produce application/json
// @Param data body api.deleteDeployAppParams true "新增公司"
// @Success 200 {string} json "{"success":true,"data":{},"msg":"删除成功"}"
// @Router /jump/filter/deleteJumpServerCmdFilter [delete]
export const deleteJumpServerCmdFilter = (data) => {
    return service({
        url: "/jump/filter/deleteJumpServerCmdFilter",
        method: 'delete',
        data
    })
}
